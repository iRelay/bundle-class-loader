# bundle-class-loader

[![Maven Central](https://img.shields.io/maven-central/v/net.relaysoft.commons/bundle-class-loader.svg?style=flat-square)](http://search.maven.org/#artifactdetails%7Cnet.relaysoft.commons%7Cbundle-class-loader%7C1.0.0%7Cjar)

## Overview

Simple custom Java Bundle Class Loader for runtime class loading from various sources.

Bundle class loader can load classes from designed files, URLs or directly from binary content provided. All loaded resources must be JAR 
or CLASS files or content loaded from such files.

Bundle class loader bundles Java class native loaders together with a custom local class loader which is used to load runtime class 
resources. This local loader is only overridden by customized boot loader which handles the loading of classes from Java native and other 
configurable boot level packages. 

Native class loaders are used for loading classes not added as bundle loader resource, but provided by class path from main application.

Bundle class loader can be configured to automatically create jdk proxies for objects created by it when created object class implements any 
interface. This enables casting loaded objects into interfaces even when the interface is loaded by different class loader than bundle 
class loader.

### Configuring boot loader

By default the boot loader only handles classes from `java.*` package. It is possible to configure boot loader also to handle classes from 
other packages like `javax.*` either through `application.properties` file or by environment variable with property name 
`relaysoft.bundle.loader.boot.packages`. Package names are given as comma separated string e.g. 
`relaysoft.bundle.loader.boot.packages = javax.*, my.custom.package.*`.

### Enabling automatic object casting

Java does not allow casting objects created by the bundle class loader of types (interfaces) loaded with different class loaders. This will 
results to ClassCastException. E.g. when API implementation is loaded with bundle class loader and the API is loaded in the current class 
loader. Bundle class loader handles this problem by allowing to create proxies from the objects loaded by the bundle class loader. By 
default bundle loader uses regular JDK proxies which enables creating proxies only for interfaces. Optionally it is possible to use 
[cglib](https://github.com/cglib/cglib) dynamic proxies which enables creating proxies for any classes. In order to use cglib proxy provider 
add  _cglib.jar_  into your project dependencies and set cglib proxy provider as your default proxy provider 
`ProxyProviderFactory.setDefaultProxyProvider(new CglibProxyProvider());`.

By default bundle class loader does not automatically create proxies for objects which implements interfaces. It is possible configure 
bundle loader to automatically create proxies from created objects through `application.properties` file or by environment variable with 
property name `relaysoft.bundle.loader.proxy.auto = true`. This can be also done programmatically by using `setAutoProxyEnabled` method.

Alternatively object casting can be done only when required with the `ProxyUtil` utility class.

```java
MyInterface myInt = (MyInterface) ProxyUtil.toCastableProxy(loader.create("path.to.MyClass"))
```

## Example

Attach data manager into your project as Maven dependency.

```xml
<dependency>
	<groupId>net.relaysoft.commons</groupId>
	<artifactId>bundle-class-loader</artifactId> 
	<version>1.0.0</version>
</dependency>
```

Create and use bundle class loader as follow:

```java
// Create new bundle class loader instance
BundleClassLoader loader = BundleClassLoaderFactory.getInstance().create("myBundleClassLoader");

// Add resource to loader
loader.addResource(new File("/path/to/file1.jar"));
// Or multiple resources at the same time
loader.addResources(Arrays.asList(new File("/path/to/file2.jar"), new File("/path/to/file3.jar")));

// Create new objects from added resources
// With default constructor
Object object1 = loader.create("path.to.MyClass");
// And with parameters
Object object2 = loader.create("path.to.MyClass", new Object[] {"test"});
Object object3 = loader.create("path.to.MyClass", new Object[] {"test", 10}, new Class<?>[] {String.class, Integer.class});

// It is also possible to use factory methods inside the bundle from loaded resources
Object object4 = loader.create("path.to.MyFactoryClass", "methodName");
// And with parameters
Object object5 = loader.create("path.to.MyFactoryClass", "methodName", new Object[] {"test"});
Object object6 = loader.create("path.to.MyFactoryClass", "methodName", new Object[] {"test", 10}, new Class<?>[] {String.class, Integer.class});

// If application class loader introduces interface which is implemented by the classes in loaded resources, then classes created with bundle loader can
// be cast into that interface. If automatic proxies are enabled casting can be made directly. Otherwise proxy utility can be used.
MyInterface myInt1 = (MyInterface) loader.create("path.to.MyClass");
MyInterface myInt2 = (MyInterface) ProxyUtil.toCastableProxy(loader.create("path.to.MyClass"));

```

`BundleClassLoaderFactory` stores created bundle class loaders in-memory and also provides methods for directly creating new objects with 
selected bundle class loader. Use factory to store multiple bundle class loader instances.

```java
BundleClassLoaderFactory factory = BundleClassLoaderFactory.getInstance();
factory.create("myBundleClassLoader");
factory.createObject("myBundleClassLoader", "newInstance"`);
```

### Spring Boot

Bundle class loader can be used with Spring Boot framework to provided bean implementations from runtime loaded jars.

One way to achieve this is to create custom application context provider and use it to register bean classes loaded with bundle class 
loader.

```java
@Component
public class ApplicationContextProvider implements ApplicationContextAware {
	
	private ApplicationContext applicationContext;

	public ApplicationContext getApplicationContext() {
        return applicationContext;
    }
	
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

}
```

Helper utility for the bean definitions.

```java
public class Customizers {

	public static void prototypeScoped(BeanDefinition bd) {
		bd.setScope(ConfigurableBeanFactory.SCOPE_PROTOTYPE);
	}

	public static void lazy(BeanDefinition bd) {
		bd.setLazyInit(true);
	}

	public static void defaultInitMethod(BeanDefinition bd) {
		bd.setInitMethodName("init");
	}

	public static void defaultDestroyMethod(BeanDefinition bd) {
		bd.setDestroyMethodName("destroy");
	}
	
	private Customizers() {}

}
```

Use the custom application provider following way:

```java
// Inject custom application context into your component
@Autowired
ApplicationContextProvider applicationContextProvider;

public void myMethod() {
	// Create new bundle class loader instance
	BundleClassLoader loader = BundleClassLoaderFactory.getInstance().create("myBundleClassLoader");
	// Add resource to loader
	loader.addResource(new File("/path/to/file1.jar"));
		
	// Initialize application context
	GenericWebApplicationContext ctx = initApplicationContext(loader);
	
	// Get bean from application context
	MyInterface myInt = (MyInterface) ctx.getBean("path.to.MyBeanClass");
}

private GenericWebApplicationContext getApplicationContext() {
	return (GenericWebApplicationContext) applicationContextProvider.getApplicationContext();
}

private GenericWebApplicationContext initApplicationContext(BundleClassLoader loader) {
	// Get web application context from custom application provider
	GenericWebApplicationContext ctx = getApplicationContext();
	// Set bundle class loader as web application context default class loader
	ctx.setClassLoader((ClassLoader) loader);
	// Register beans
	ctx.registerBean(loader.create("path.to.MyBeanClass").getClass(), Customizers::prototypeScoped, Customizers::lazy);
}

```

## Getting Started with development

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Java version 8

### Installing

Just checkout sources from GitLab.

## Running the tests

Project unit tests are executed within Maven _test_.

Unit tests:

	mvn test

## Deployment

Project can be added as a part of existing application as Maven dependency.

```xml
<dependency>
	<groupId>net.relaysoft.commons</groupId>
	<artifactId>bundle-class-loader</artifactId> 
	<version>1.0.0</version>
</dependency>
```

Create new bundle class loader `BundleClassLoader` instance and start adding `jar` and/or `class` resources in it.

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on the process for submitting pull requests to us.

## Versioning

Uses [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/irelay/bundle-class-loader/tags). 

## Authors

* **Tapani Leskinen** - [relaysoft.net](https://relaysoft.net)

## License

This project is licensed under the Apache License 2.0 - see the [LICENSE](LICENSE) file for details