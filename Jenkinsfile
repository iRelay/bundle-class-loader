pipeline {
	agent any
  	tools {
    	jdk "jdk8"
    	maven "maven"
  	}
  	options {
  		buildDiscarder(logRotator(numToKeepStr:'5'))
    	timeout(time: 15, unit: 'MINUTES')
  	}
  	post {
    	always {
	      archive 'target/bundle-class-loader-*.jar'
	      junit 'target/surefire-reports/*.xml'
	      deleteDir()
    	}   
  	}
  	environment {
  		VERSION = '0.0.0'
  		NAME = ''
  		GIT_URL = 'gitlab.com/iRelay/bundle-class-loader.git'
  	}
  	stages {
	    stage("Prepare") {
	        steps {
	        	checkout scm
	        	script {
		            if(env.BRANCH_NAME == 'master') {
		            	configFileProvider([configFile(fileId: 'maven-settings', variable: 'MAVEN_SETTINGS')]) {
	          				sh "mvn versions:set -DremoveSnapshot -s $MAVEN_SETTINGS"
	          				sh "mvn versions:use-releases -s $MAVEN_SETTINGS"
	       				}
	       				withCredentials([[$class: 'UsernamePasswordMultiBinding',
	          			credentialsId: 'gitlab',
	             		usernameVariable: 'GIT_USERNAME',
	             		passwordVariable: 'GIT_PASSWORD']]) {
	            			sh "git config user.email \"${env.GIT_USERNAME}\""
	             			sh "git config user.name \"${env.GIT_USERNAME}\""
	             			sh "git add pom.xml"
	             			sh "git commit -m \"Updated pom.xml into release version\""	
	           	 		}
		            }
		            def pom = readMavenPom file: 'pom.xml'
	        		NAME = pom.artifactId
	        		VERSION = pom.version
		    	}
	        }
	    }
	    stage("Build") {
	    	steps {
	     		configFileProvider([configFile(fileId: 'maven-settings', variable: 'MAVEN_SETTINGS')]) {
	     			sh "mvn clean package -Dskip.surefire.tests -Djacoco.skip=true -s $MAVEN_SETTINGS"
	     		}
	      	}
	    }
	    stage("Test") {
	    	steps {
	     		configFileProvider([configFile(fileId: 'maven-settings', variable: 'MAVEN_SETTINGS')]) {
	    			sh "mvn verify -Dskip.generate.operations -Dskip.attach.operations -Djacoco.skip=true -s $MAVEN_SETTINGS"
	    		}
	      	}
	    }
	    stage('Inspect') {
	    	steps {
	  			configFileProvider([configFile(fileId: 'maven-settings', variable: 'MAVEN_SETTINGS')]) {
	       			sh "mvn sonar:sonar -Dsonar.scm.disabled=true -s $MAVEN_SETTINGS"
	      		}
	      	}
	    }
	    stage("Release"){
	    	steps {
	    	   script {
		            if(env.BRANCH_NAME == 'master') {
	    				configFileProvider([configFile(fileId: 'maven-settings', variable: 'MAVEN_SETTINGS')]) {
	      					sh "mvn verify -Dskip.surefire.tests -Dskip.integration.tests -Dskip.attach.operations -Djacoco.skip=true -s $MAVEN_SETTINGS"			
		            	}
		            	sh "cp pom.xml target/${NAME}-${VERSION}.pom"
		            	try{
		            		sh "gpg-agent --daemon"
		            	} catch (error) {
			            	echo ('Expected error from GPG')            
			            }
		            	withCredentials([[$class: 'UsernamePasswordMultiBinding',
	          			credentialsId: 'gpg',
			            usernameVariable: 'GPG_USERNAME',
			            passwordVariable: 'GPG_PASSWORD']]) {
			            	try{
			            	    sh "gpg --homedir /var/jenkins_home/.gnupg --batch --no-tty --passphrase ${env.GPG_PASSWORD} -ab target/${NAME}-${VERSION}.pom"
			            	} catch (error) {
			            		echo ('Expected error from GPG')            
			            	}
			            	try{
			            	    sh "gpg --homedir /var/jenkins_home/.gnupg --batch --no-tty --passphrase ${env.GPG_PASSWORD} -ab target/${NAME}-${VERSION}.jar"
			            	} catch (error) {
			            		echo ('Expected error from GPG')            
			            	}
							try{
			            	    sh "gpg --homedir /var/jenkins_home/.gnupg --batch --no-tty --passphrase ${env.GPG_PASSWORD} -ab target/${NAME}-${VERSION}-javadoc.jar"
			            	} catch (error) {
			            		echo ('Expected error from GPG')            
			            	}
			            	try{
			            	    sh "gpg --homedir /var/jenkins_home/.gnupg --batch --no-tty --passphrase ${env.GPG_PASSWORD} -ab target/${NAME}-${VERSION}-sources.jar"
			            	} catch (error) {
			            		echo ('Expected error from GPG')            
			            	}
	    				}
	    				configFileProvider([configFile(fileId: 'maven-settings', variable: 'MAVEN_SETTINGS')]) {
	      					sh "mvn -Possrh deploy -Dskip.surefire.tests -Dskip.integration.tests -Dskip.generate.operations -Djacoco.skip=true -s $MAVEN_SETTINGS"			
		            	}
		            	withCredentials([[$class: 'UsernamePasswordMultiBinding',
	          			credentialsId: 'gitlab',
			            usernameVariable: 'GIT_USERNAME',
			            passwordVariable: 'GIT_PASSWORD']]) {
				             	sh "git config user.email \"${env.GIT_USERNAME}\""
				             	sh "git config user.name \"${env.GIT_USERNAME}\""
				             	sh "git tag -a v${VERSION} -m \"Version ${VERSION} release\""
				             	sh "git push https://${env.GIT_USERNAME}:${env.GIT_PASSWORD}@${GIT_URL} v${VERSION}"
	    				}	
					}		
		            else {	
	    				configFileProvider([configFile(fileId: 'maven-settings', variable: 'MAVEN_SETTINGS')]) {
	      					sh "mvn deploy -Dskip.surefire.tests -Dskip.integration.tests -Dskip.generate.operations -Dskip.attach.operations -Djacoco.skip=true -s $MAVEN_SETTINGS"			
		            	}
		            }
	        	} 
	    	}     
	    }
	}
}