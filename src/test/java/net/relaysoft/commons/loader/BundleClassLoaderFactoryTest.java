package net.relaysoft.commons.loader;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BundleClassLoaderFactoryTest {

	private static final String INTERNAL_NAME = "internalTestLoader";
	private static final String EXTERNAL_NAME = "externalTestLoader";

	private static final String TEST_INTERNAL_CLASS_NAME = "test.InternalTestImpl";
	private static final String TEST_EXTERNAL_CLASS_NAME = "test.ExternalTestImpl";

	private static final String TEST_INTERNAL_FACTORY_CLASS_NAME = "test.InternalTestFactory";
	private static final String TEST_EXTERNAL_FACTORY_CLASS_NAME = "test.ExternalTestFactory";

	private static final String TEST_INTERNAL_JAR_PATH = "jars/bundle-loader-internal-test.jar";
	private static final String TEST_EXTERNAL_JAR_PATH = "jars/bundle-loader-external-test.jar";

	private static final List<String> EXPECTED_LOADED_INTERNAL_RESOURCES = Arrays.asList( 
			"test/InternalTestImpl.class", 
			"test/InternalTestFactory.class", 
			"META-INF/maven/test/bundle-loader-internal-test/pom.properties", 
			"META-INF/maven/test/bundle-loader-internal-test/pom.xml"
			);

	private static final List<String> EXPECTED_LOADED_EXTERNAL_RESOURCES = Arrays.asList(
			"test/ExternalTestImpl.class", 
			"test/ExternalTestFactory.class",
			"META-INF/maven/test/bundle-loader-external-test/pom.properties", 
			"META-INF/maven/test/bundle-loader-external-test/pom.xml"
			);

	private BundleClassLoaderFactory factory;

	private File testExternalJar;
	private File testInternalJar;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		testExternalJar = new File(TEST_EXTERNAL_JAR_PATH);
		testInternalJar = new File(TEST_INTERNAL_JAR_PATH);
		initializeFactory();
	}

	private void initializeFactory() {
		factory = BundleClassLoaderFactory.getInstance();
		assertTrue(factory.getBundleClassLoaderNames().isEmpty());
		factory.initializeBundleClassloader(INTERNAL_NAME);
		assertAll("Test initialized loaders", 
				() -> assertEquals(1, factory.getBundleClassLoaderNames().size()),
				() -> assertEquals(INTERNAL_NAME, factory.getBundleClassLoaderNames().get(0))
				);

	}

	@AfterEach
	void tearDown() throws Exception {
		factory.terminateBundleClassloaders();
	}

	@Test
	void testGetInstance() {
		assertAll("Test getting factory instance", 
				() -> assertNotNull(BundleClassLoaderFactory.getInstance()),
				() -> assertEquals(factory, BundleClassLoaderFactory.getInstance())
				);
	}

	@Test
	void testAddResourceToBundleClassLoaderFromByteArray() throws IOException {
		factory.addResourceToBundleClassLoader(INTERNAL_NAME, Files.readAllBytes(testInternalJar.toPath()));
		factory.addResourceToBundleClassLoader(EXTERNAL_NAME, Files.readAllBytes(testExternalJar.toPath()));
		assertAll("Test resource loaded", 
				() -> assertThat(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedResourceNames(), 
						containsInAnyOrder(EXPECTED_LOADED_INTERNAL_RESOURCES.toArray())),
				() -> assertThat(factory.getBundleClassLoader(EXTERNAL_NAME).getLoadedResourceNames(), 
						containsInAnyOrder(EXPECTED_LOADED_EXTERNAL_RESOURCES.toArray()))
				);
	}

	@Test
	void testAddResourceToBundleClassLoaderFromFile() throws IOException {
		factory.addResourceToBundleClassLoader(INTERNAL_NAME, testInternalJar);
		factory.addResourceToBundleClassLoader(EXTERNAL_NAME, testExternalJar);
		assertAll("Test resource loaded", 
				() -> assertThat(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedResourceNames(), 
						containsInAnyOrder(EXPECTED_LOADED_INTERNAL_RESOURCES.toArray())),
				() -> assertThat(factory.getBundleClassLoader(EXTERNAL_NAME).getLoadedResourceNames(), 
						containsInAnyOrder(EXPECTED_LOADED_EXTERNAL_RESOURCES.toArray()))
				);
	}

	@Test
	void testAddResourceToBundleClassLoaderFromURL() throws IOException {
		factory.addResourceToBundleClassLoader(INTERNAL_NAME, testInternalJar.toURI().toURL());
		factory.addResourceToBundleClassLoader(EXTERNAL_NAME, testExternalJar.toURI().toURL());
		assertAll("Test resource loaded", 
				() -> assertThat(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedResourceNames(), 
						containsInAnyOrder(EXPECTED_LOADED_INTERNAL_RESOURCES.toArray())),
				() -> assertThat(factory.getBundleClassLoader(EXTERNAL_NAME).getLoadedResourceNames(), 
						containsInAnyOrder(EXPECTED_LOADED_EXTERNAL_RESOURCES.toArray()))
				);
	}

	@Test
	void testAddResourcesToBundleClassLoader() throws IOException {
		factory.addResourcesToBundleClassLoader(INTERNAL_NAME, Arrays.asList(testExternalJar, testInternalJar));
		List<String> expectedNames = Stream.of(EXPECTED_LOADED_EXTERNAL_RESOURCES, EXPECTED_LOADED_INTERNAL_RESOURCES)
				.flatMap(x -> x.stream())
				.collect(Collectors.toList());
		assertThat(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedResourceNames(), containsInAnyOrder(expectedNames.toArray()));
	}

	@Test
	void testCreateObjectWithDefaultConstructor() throws IOException {
		factory.addResourceToBundleClassLoader(INTERNAL_NAME, testInternalJar);
		factory.addResourceToBundleClassLoader(EXTERNAL_NAME, testExternalJar);
		test.Test testInternal = (test.Test) factory.createObject(INTERNAL_NAME, TEST_INTERNAL_CLASS_NAME);
		test.Test testExternal = (test.Test) factory.createObject(EXTERNAL_NAME, TEST_EXTERNAL_CLASS_NAME);
		assertAll("Test internal implementation", 
				() -> assertNotNull(testInternal),
				() -> assertNull(testInternal.get()),
				() -> testInternal.set(""),
				() -> assertEquals("", testInternal.get()),
				() -> testInternal.set("test"),
				() -> assertEquals("test", testInternal.get())
				);
		assertAll("Test external implementation", 
				() -> assertNotNull(testExternal),
				() -> assertEquals("unknown", testExternal.get()),
				() -> testExternal.set(""),
				() -> assertEquals("default", testExternal.get()),
				() -> testExternal.set("test"),
				() -> assertEquals("test", testExternal.get())
				);
	}

	@Test
	void testCreateObjectWithDefaultFactoryMethod() throws IOException {
		factory.addResourceToBundleClassLoader(INTERNAL_NAME, testInternalJar);
		factory.addResourceToBundleClassLoader(EXTERNAL_NAME, testExternalJar);
		test.Test testInternal = (test.Test) factory.createObject(INTERNAL_NAME, TEST_INTERNAL_FACTORY_CLASS_NAME, "newInstance");
		test.Test testExternal = (test.Test) factory.createObject(EXTERNAL_NAME, TEST_EXTERNAL_FACTORY_CLASS_NAME, "newInstance");
		assertAll("Test internal implementation", 
				() -> assertNotNull(testInternal),
				() -> assertNull(testInternal.get()),
				() -> testInternal.set(""),
				() -> assertEquals("", testInternal.get()),
				() -> testInternal.set("test"),
				() -> assertEquals("test", testInternal.get()),
				() -> assertThat(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedClassNames(), 
						containsInAnyOrder(Arrays.asList(TEST_INTERNAL_FACTORY_CLASS_NAME, TEST_INTERNAL_CLASS_NAME).toArray()))
				);
		assertAll("Test external implementation", 
				() -> assertNotNull(testExternal),
				() -> assertEquals("unknown", testExternal.get()),
				() -> testExternal.set(""),
				() -> assertEquals("default", testExternal.get()),
				() -> testExternal.set("test"),
				() -> assertEquals("test", testExternal.get()),
				() -> assertThat(factory.getBundleClassLoader(EXTERNAL_NAME).getLoadedClassNames(), 
						containsInAnyOrder(Arrays.asList(TEST_EXTERNAL_FACTORY_CLASS_NAME, TEST_EXTERNAL_CLASS_NAME).toArray()))
				);
	}

	@Test
	void testCreateObjectWithCustomConstructor() throws IOException {
		factory.addResourceToBundleClassLoader(INTERNAL_NAME, testInternalJar);
		factory.addResourceToBundleClassLoader(EXTERNAL_NAME, testExternalJar);
		test.Test testInternal = (test.Test) factory.createObject(INTERNAL_NAME, TEST_INTERNAL_CLASS_NAME, new Object[] {"test"}, 
				new Class[] {String.class});
		test.Test testExternal = (test.Test) factory.createObject(EXTERNAL_NAME, TEST_EXTERNAL_CLASS_NAME, new Object[] {"test"}, 
				new Class[] {String.class});
		assertAll("Test internal implementation", 
				() -> assertNotNull(testInternal),
				() -> assertEquals("test", testInternal.get()),
				() -> testInternal.set(""),
				() -> assertEquals("", testInternal.get()),
				() -> testInternal.set("test"),
				() -> assertEquals("test", testInternal.get())
				);	
		assertAll("Test external implementation", 
				() -> assertNotNull(testExternal),
				() -> assertEquals("test", testExternal.get()),
				() -> testExternal.set(""),
				() -> assertEquals("default", testExternal.get()),
				() -> testExternal.set("test"),
				() -> assertEquals("test", testExternal.get())
				);
		
	}

	@Test
	void testCreateObjectWithCustomFactoryMethod() throws IOException {
		factory.addResourceToBundleClassLoader(INTERNAL_NAME, testInternalJar);
		factory.addResourceToBundleClassLoader(EXTERNAL_NAME, testExternalJar);
		test.Test testInternal = (test.Test) factory.createObject(INTERNAL_NAME, TEST_INTERNAL_FACTORY_CLASS_NAME, "newInstance", 
				new Object[] {"test"}, new Class[] {String.class});
		test.Test testExternal = (test.Test) factory.createObject(EXTERNAL_NAME, TEST_EXTERNAL_FACTORY_CLASS_NAME, "newInstance", 
				new Object[] {"test"}, new Class[] {String.class});
		assertAll("Test internal implementation", 
				() -> assertNotNull(testInternal),
				() -> assertEquals("test", testInternal.get()),
				() -> testInternal.set(""),
				() -> assertEquals("", testInternal.get()),
				() -> testInternal.set("test"),
				() -> assertEquals("test", testInternal.get()),
				() -> assertThat(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedClassNames(), 
						containsInAnyOrder(Arrays.asList(TEST_INTERNAL_FACTORY_CLASS_NAME, TEST_INTERNAL_CLASS_NAME).toArray()))
				);
		assertAll("Test external implementation", 
				() -> assertNotNull(testExternal),
				() -> assertEquals("test", testExternal.get()),
				() -> testExternal.set(""),
				() -> assertEquals("default", testExternal.get()),
				() -> testExternal.set("test"),
				() -> assertEquals("test", testExternal.get()),
				() -> assertThat(factory.getBundleClassLoader(EXTERNAL_NAME).getLoadedClassNames(), 
						containsInAnyOrder(Arrays.asList(TEST_EXTERNAL_FACTORY_CLASS_NAME, TEST_EXTERNAL_CLASS_NAME).toArray()))
				);
	}

	@Test
	void testGetBundleClassLoader() {
		assertAll("Test bundle class loader instances", 
				() -> assertNotNull(factory.getBundleClassLoader(INTERNAL_NAME)),
				() -> assertEquals(INTERNAL_NAME, factory.getBundleClassLoader(INTERNAL_NAME).getName()),
				() -> assertNotNull(factory.getBundleClassLoader(EXTERNAL_NAME)),
				() -> assertEquals(EXTERNAL_NAME, factory.getBundleClassLoader(EXTERNAL_NAME).getName())
				);
	}

	@Test
	void testInitializeBundleClassloader() throws IOException {
		factory.addResourceToBundleClassLoader(INTERNAL_NAME, testInternalJar);
		assertFalse(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedResourceNames().isEmpty());
		factory.initializeBundleClassloader(INTERNAL_NAME);
		factory.initializeBundleClassloader(EXTERNAL_NAME);
		assertTrue(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedResourceNames().isEmpty());
		assertTrue(factory.getBundleClassLoader(EXTERNAL_NAME).getLoadedResourceNames().isEmpty());
	}

	@Test
	void testInitializeBundleClassloaderWithOptionalParameters() throws IOException {
		factory.addResourceToBundleClassLoader(INTERNAL_NAME, testInternalJar);
		assertFalse(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedResourceNames().isEmpty());
		factory.initializeBundleClassloader(INTERNAL_NAME, getClass().getClassLoader());
		assertTrue(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedResourceNames().isEmpty());
		factory.initializeBundleClassloader(INTERNAL_NAME, getClass().getClassLoader(), "javax.*");
		assertTrue(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedResourceNames().isEmpty());
		factory.initializeBundleClassloader(INTERNAL_NAME, getClass().getClassLoader(), new Properties());
		assertTrue(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedResourceNames().isEmpty());
		factory.initializeBundleClassloader(INTERNAL_NAME, getClass().getClassLoader(), new Properties(), "javax.*");
		assertTrue(factory.getBundleClassLoader(INTERNAL_NAME).getLoadedResourceNames().isEmpty());
	}


	@Test
	void testTerminateBundleClassloader() {
		assertFalse(factory.getBundleClassLoaderNames().isEmpty());
		factory.terminateBundleClassloader(INTERNAL_NAME);
		assertTrue(factory.getBundleClassLoaderNames().isEmpty());
	}
	
	@Test
	void testTerminateBundleClassloaders() throws IOException {
		factory.addResourceToBundleClassLoader(INTERNAL_NAME, testInternalJar);
		factory.addResourceToBundleClassLoader(EXTERNAL_NAME, testExternalJar);
		assertFalse(factory.getBundleClassLoaderNames().isEmpty());
		factory.terminateBundleClassloaders();
		assertTrue(factory.getBundleClassLoaderNames().isEmpty());
	}

}
