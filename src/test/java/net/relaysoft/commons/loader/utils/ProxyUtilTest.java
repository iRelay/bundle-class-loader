package net.relaysoft.commons.loader.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Proxy;

import org.junit.jupiter.api.Test;

import test.TestPojo;

class ProxyUtilTest {

	@Test
	void testToCastableProxy() throws Exception {
		Object obj = ProxyUtil.toCastableProxy(getTestPojoObject());
		assertTrue(obj instanceof Proxy);
	}

	@Test
	void testToCastableProxyWithCustomClassLoader() throws Exception {
		Object obj = ProxyUtil.toCastableProxy(getTestPojoObject(), this.getClass().getClassLoader());
		assertTrue(obj instanceof Proxy);
	}
	
	private Object getTestPojoObject() throws Exception {
		return Class.forName(TestPojo.class.getName()).newInstance();
	}

}
