package net.relaysoft.commons.loader.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Properties;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PropertyUtilTest {

	@BeforeEach
	void setUp() throws Exception {
		System.setProperty("test.property.two", "xxx");
		System.setProperty("relaysoft.bundle.loader.two", "xxx");
	}

	@AfterEach
	void tearDown() throws Exception {
		System.setProperty("test.property.two", "");
		System.setProperty("relaysoft.bundle.loader.two", "");
	}

	@Test
	void testLoadProperties() {
		Properties properties = PropertyUtil.loadProperties();
		assertAll("properties",
				() -> assertNotNull(properties),
				() -> assertEquals("test1", properties.get("test.property.one")),
				() -> assertEquals("test2", properties.get("test.property.two")),
				() -> assertEquals("loader1", properties.get("relaysoft.bundle.loader.one")),
				() -> assertEquals("xxx", properties.get("relaysoft.bundle.loader.two")),
				() -> assertEquals("x, y, z", properties.get("relaysoft.bundle.loader.array"))
				);

	}

	@Test
	void testGetAsStringArray() {
		Properties properties = PropertyUtil.loadProperties();
		assertAll("properties",
				() -> assertArrayEquals(new String[] {"test1"}, PropertyUtil.getAsStringArray(properties, "test.property.one")),
				() -> assertArrayEquals(new String[] {"x", "y", "z"}, 
						PropertyUtil.getAsStringArray(properties, "relaysoft.bundle.loader.array"))
				);
	}

}
