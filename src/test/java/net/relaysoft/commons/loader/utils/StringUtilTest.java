package net.relaysoft.commons.loader.utils;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

class StringUtilTest {

	@ParameterizedTest
	@NullAndEmptySource
	@ValueSource(strings = {"  ", "\t", "\n"})
	void testIsBlank(String input) {
		assertTrue(StringUtil.isBlank(input));
	}

	@ParameterizedTest
	@NullAndEmptySource
	@ValueSource(strings = {"  ", "\t", "\n"})
	void testIsNotBlank(String input) {
		assertFalse(StringUtil.isNotBlank(input));
	}

	@ParameterizedTest
	@CsvSource({"test*,^test.*$", 
		"test?,^test.$",
		"(test),^\\(test\\)$",
		"[test],^\\[test\\]$",
		"{test},^\\{test\\}$",
		"$test,^\\$test$",
		"^test,^\\^test$",
		"test.,^test\\.$",		
		"te|st,^te\\|st$",
		"\\test,^\\\\test$",	
		})
	void testToRegex(String input, String expected) {
		assertEquals(expected, StringUtil.toRegex(input));
	}

}
