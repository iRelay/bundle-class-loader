package net.relaysoft.commons.loader.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class StreamUtilTest {
	
	private static final String TEST_CONTENT = "test content";
	
	private InputStream is;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		is = new ByteArrayInputStream(TEST_CONTENT.getBytes(StandardCharsets.UTF_8));
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testToByteArray() throws IOException {
		byte[] result = StreamUtil.toByteArray(is);
		assertEquals(TEST_CONTENT, new String(result));
	}

	@ParameterizedTest
	@MethodSource("provideValuesForTest")
	void testToByteArrayWhenSizeIsGiven(long size, String expectedValue) throws IOException {
		byte[] result = StreamUtil.toByteArray(is, size);
		assertEquals(expectedValue, new String(result));
	}
	
	@Test
	void testToByteArrayWhenSizeIsTooBig() throws IOException {
		assertThrows(IllegalArgumentException.class, () -> StreamUtil.toByteArray(is, Long.MAX_VALUE));
	}
	
	private static Stream<Arguments> provideValuesForTest() {
		return Stream.of(
				Arguments.of(-1, TEST_CONTENT),
				Arguments.of(0, ""),
				Arguments.of(4, "test")
				);
	}

}
