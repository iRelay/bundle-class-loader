package net.relaysoft.commons.loader.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class ClassNameUtilTest {

	@ParameterizedTest
	@MethodSource("provideValuesForTest")
	void testClassNameToPathName(String className, String expectedPath) {
		assertEquals(expectedPath, ClassNameUtil.classNameToPathName(className));
	}
	
	private static Stream<Arguments> provideValuesForTest() {
		return Stream.of(
				Arguments.of(null, null),
				Arguments.of("", ""),
				Arguments.of("TestClass", "TestClass.class"),
				Arguments.of("net.relaysoft.test.TestClass", "net/relaysoft/test/TestClass.class"),
				Arguments.of("src/test/net.relaysoft.test.TestClass", "src/test/net/relaysoft/test/TestClass.class")
				);
	}

}
