package net.relaysoft.commons.loader.utils;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.IOException;

import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class FileNameUtilTest {

	@SuppressWarnings("static-access")
	@ParameterizedTest
	@ValueSource(strings = {"", "jar"})
	void testGetExtension(String extension, @TempDir File tempDir) throws IOException {	
		File file = tempDir.createTempFile("test", !extension.trim().isEmpty() ? ".".concat(extension) : extension);
		assertEquals(extension, FileNameUtil.getExtension(file));
	}

}
