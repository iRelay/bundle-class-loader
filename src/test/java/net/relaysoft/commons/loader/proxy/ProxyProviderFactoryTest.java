package net.relaysoft.commons.loader.proxy;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProxyProviderFactoryTest {

	@BeforeEach
	void setUp() throws Exception {
		ProxyProviderFactory.setDefaultProxyProvider(null);
	}
	
	@Test
	void testCreate() {
		assertTrue(ProxyProviderFactory.create() instanceof DefaultProxyProvider);
	}

	@Test
	void testSetDefaultProxyProvider() {
		ProxyProviderFactory.setDefaultProxyProvider(new CglibProxyProvider());
		assertTrue(ProxyProviderFactory.create() instanceof CglibProxyProvider);
	}

}
