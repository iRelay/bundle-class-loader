package net.relaysoft.commons.loader;

import static org.junit.jupiter.api.Assertions.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import net.relaysoft.commons.loader.exceptions.BundleClassLoaderException;
import net.relaysoft.commons.loader.proxy.CglibProxyProvider;
import net.relaysoft.commons.loader.proxy.ProxyProviderFactory;

class BundleClassLoaderTest {

	private static final String NAME = "test";
	
	private static final String TEST_INTERNAL_CLASS_NAME = "test.InternalTestImpl";
	private static final String TEST_EXTERNAL_CLASS_NAME = "test.ExternalTestImpl";
	private static final String TEST_ABSTRACT_CLASS_NAME = "test.AbstractTestImpl";
	
	private static final String TEST_INTERNAL_FACTORY_CLASS_NAME = "test.InternalTestFactory";
	private static final String TEST_EXTERNAL_FACTORY_CLASS_NAME = "test.ExternalTestFactory";

	private static final String TEST_INTERNAL_JAR_PATH = "jars/bundle-loader-internal-test.jar";
	private static final String TEST_EXTERNAL_JAR_PATH = "jars/bundle-loader-external-test.jar";
	private static final String TEST_ABSTRACT_JAR_PATH = "jars/bundle-loader-abstract-test.jar";

	private static final List<String> EXPECTED_LOADED_INTERNAL_RESOURCES = Arrays.asList( 
			"test/InternalTestImpl.class", 
			"test/InternalTestFactory.class", 
			"META-INF/maven/test/bundle-loader-internal-test/pom.properties", 
			"META-INF/maven/test/bundle-loader-internal-test/pom.xml"
			);
	
	private static final List<String> EXPECTED_LOADED_EXTERNAL_RESOURCES = Arrays.asList(
			"test/ExternalTestImpl.class", 
			"test/ExternalTestFactory.class",
			"META-INF/maven/test/bundle-loader-external-test/pom.properties", 
			"META-INF/maven/test/bundle-loader-external-test/pom.xml"
			);

	private BundleClassLoader bundleClassLoader;

	private File testExternalJar;
	private File testInternalJar;
	private File testAbstractJar;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		testExternalJar = new File(TEST_EXTERNAL_JAR_PATH);
		testInternalJar = new File(TEST_INTERNAL_JAR_PATH);
		testAbstractJar = new File(TEST_ABSTRACT_JAR_PATH);
		bundleClassLoader = new BundleClassLoaderImpl(NAME);
	}

	@AfterEach
	void tearDown() throws Exception {
		if(bundleClassLoader != null) {
			bundleClassLoader.unloadAllClasses();
		}
		ProxyProviderFactory.setDefaultProxyProvider(null);
	}
	
	@ParameterizedTest
	@MethodSource("provideCostructorParameters")
	void testBundleClassLoader(ClassLoader parent, Properties properties, String[] bootPackages) {
		assertAll("Test constructors", 
			() -> assertNotNull(new BundleClassLoaderImpl(NAME, parent, properties, bootPackages)),
			() -> assertNotNull(new BundleClassLoaderImpl(NAME, parent, properties)),
			() -> assertNotNull(new BundleClassLoaderImpl(NAME, parent)),
			() -> assertNotNull(new BundleClassLoaderImpl(NAME, parent, bootPackages)),
			() -> assertNotNull(new BundleClassLoaderImpl(NAME, bootPackages)),
			() -> assertNotNull(new BundleClassLoaderImpl(NAME, properties)),
			() -> assertNotNull(new BundleClassLoaderImpl(NAME, properties, bootPackages)),
			() -> assertNotNull(new BundleClassLoaderImpl(NAME))
		);
		
	}
	
	private static Stream<Arguments> provideCostructorParameters() {
	    return Stream.of(
	      Arguments.of(new URLClassLoader(new URL[] {}), new Properties(), new String[] {"test.*"}),
	      Arguments.of(new URLClassLoader(new URL[] {}), new Properties(), null),
	      Arguments.of(new URLClassLoader(new URL[] {}), null, null),
	      Arguments.of(null, null, null)
	    );
	}

	
	@Test
	void testSetAutoProxyEnabled() throws IOException {
		bundleClassLoader.addResource(testInternalJar);
		assertAll("Test auto proxy when not enabled", 
				() -> assertFalse(bundleClassLoader.isAutoProxyEnabled()),
				() -> assertEquals(TEST_INTERNAL_CLASS_NAME, bundleClassLoader.create(TEST_INTERNAL_CLASS_NAME).getClass().getName()),
				() -> assertNull(((test.Test)bundleClassLoader.create(TEST_INTERNAL_CLASS_NAME)).get())
		);
		bundleClassLoader.setAutoProxyEnabled(true);
		assertAll("Test auto proxy when enabled", 
				() -> assertTrue(bundleClassLoader.isAutoProxyEnabled()),
				() -> assertNotEquals(TEST_INTERNAL_CLASS_NAME, bundleClassLoader.create(TEST_INTERNAL_CLASS_NAME).getClass().getName()),
				() -> assertTrue(bundleClassLoader.create(TEST_INTERNAL_CLASS_NAME).getClass().getName().contains("Proxy")),
				() -> assertNull(((test.Test)bundleClassLoader.create(TEST_INTERNAL_CLASS_NAME)).get())
		);
	}

	@Test
	void testAddResourceFromByteArray() throws IOException {
		byte[] fileBytes = Files.readAllBytes(testInternalJar.toPath());
		bundleClassLoader.addResource(fileBytes);
		assertThat(bundleClassLoader.getLoadedResourceNames(), containsInAnyOrder(EXPECTED_LOADED_INTERNAL_RESOURCES.toArray()));
	}

	@Test
	void testAddResourceFromFile() throws IOException {
		bundleClassLoader.addResource(testInternalJar);
		assertThat(bundleClassLoader.getLoadedResourceNames(), containsInAnyOrder(EXPECTED_LOADED_INTERNAL_RESOURCES.toArray()));
	}

	@Test
	void testAddResourceFromURL() throws MalformedURLException, IOException {
		bundleClassLoader.addResource(testInternalJar.toURI().toURL());
		assertThat(bundleClassLoader.getLoadedResourceNames(), containsInAnyOrder(EXPECTED_LOADED_INTERNAL_RESOURCES.toArray()));
	}

	@Test
	void testAddResources() throws IOException {
		bundleClassLoader.addResources(Arrays.asList(testExternalJar, testInternalJar));
		List<String> expectedNames = Stream.of(EXPECTED_LOADED_EXTERNAL_RESOURCES, EXPECTED_LOADED_INTERNAL_RESOURCES)
				.flatMap(x -> x.stream())
                .collect(Collectors.toList());
		assertThat(bundleClassLoader.getLoadedResourceNames(), containsInAnyOrder(expectedNames.toArray()));
	}
	
	@Test
	void testCreateWhenClassDoesNotExists() throws IOException {
		loadTestResources();
		assertAll("exceptions",
				() -> assertThrows(BundleClassLoaderException.class, () -> bundleClassLoader.create("test.NotFoundTestImpl")),
				() -> assertThrows(BundleClassLoaderException.class, () -> bundleClassLoader.create("test.NotFoundTestImpl", 
						new Object[] {"test"})),
				() -> assertThrows(BundleClassLoaderException.class, () -> bundleClassLoader.create("test.NotFoundTestImpl", 
						new Object[] {"test"}, new Class[] {String.class})),
				() -> assertThrows(BundleClassLoaderException.class, () -> bundleClassLoader.create("test.NotFoundTestImpl", 
						new Object[] {"test"}, new Class[] {String.class}))
		);
	}
	
	@Test
	void testCreateWhenFactoryClassDoesNotExists() throws IOException {
		loadTestResources();
		assertAll("exceptions",
				() -> assertThrows(BundleClassLoaderException.class, () -> bundleClassLoader.create("test.NotExistFactory", 
						"newInstance")),
				() -> assertThrows(BundleClassLoaderException.class, () -> bundleClassLoader.create("test.NotExistFactory", 
						"newInstance", new Object[] {"test"})),
				() -> assertThrows(BundleClassLoaderException.class, () -> bundleClassLoader.create("test.NotExistFactory", 
						"newInstance", new Object[] {"test"}, new Class[] {String.class}))
		);
	}
	
	@Test
	void testCreateWhenFactoryMethodDoesNotExists() throws IOException {
		loadTestResources();
		assertAll("exceptions",
				() -> assertThrows(BundleClassLoaderException.class, () -> bundleClassLoader.create("test.InternalTestFactory", 
						"notExistMethod")),
				() -> assertThrows(BundleClassLoaderException.class, () -> bundleClassLoader.create("test.InternalTestFactory", 
						"notExistMethod", new Object[] {"test"})),
				() -> assertThrows(BundleClassLoaderException.class, () -> bundleClassLoader.create("test.InternalTestFactory", 
						"notExistMethod", new Object[] {"test"}, new Class[] {String.class}))
		);
	}

	@ParameterizedTest
	@ValueSource(strings = {TEST_EXTERNAL_CLASS_NAME, TEST_INTERNAL_CLASS_NAME})
	void testCreateWithDefaultConstructor(String className) throws IOException {
		loadTestResources();
		test.Test test = (test.Test) bundleClassLoader.create(className);
		assertNotNull(test);
		if(className.equals(TEST_EXTERNAL_CLASS_NAME)) {
			assertAll("Test external implementation", 
					() -> assertNotNull(test),
					() -> assertEquals("unknown", test.get()),
					() -> test.set(""),
					() -> assertEquals("default", test.get()),
					() -> test.set("test"),
					() -> assertEquals("test", test.get())
					);
		} else {
			assertAll("Test internal implementation", 
					() -> assertNotNull(test),
					() -> assertNull(test.get()),
					() -> test.set(""),
					() -> assertEquals("", test.get()),
					() -> test.set("test"),
					() -> assertEquals("test", test.get())
					);
		}
		assertThat(bundleClassLoader.getLoadedClassNames(), containsInAnyOrder(Arrays.asList(className).toArray()));
	}
	
	@ParameterizedTest
	@ValueSource(strings = {TEST_EXTERNAL_FACTORY_CLASS_NAME, TEST_INTERNAL_FACTORY_CLASS_NAME})
	void testCreateWithDefaultFactoryMethod(String factoryClassName) throws IOException {
		loadTestResources();
		test.Test test = (test.Test) bundleClassLoader.create(factoryClassName, "newInstance");
		assertNotNull(test);
		if(factoryClassName.equals(TEST_EXTERNAL_FACTORY_CLASS_NAME)) {
			assertAll("Test external implementation", 
					() -> assertNotNull(test),
					() -> assertEquals("unknown", test.get()),
					() -> test.set(""),
					() -> assertEquals("default", test.get()),
					() -> test.set("test"),
					() -> assertEquals("test", test.get()),
					() -> assertThat(bundleClassLoader.getLoadedClassNames(), 
							containsInAnyOrder(Arrays.asList(TEST_EXTERNAL_FACTORY_CLASS_NAME, TEST_EXTERNAL_CLASS_NAME).toArray()))
					);
		} else {
			assertAll("Test internal implementation", 
					() -> assertNotNull(test),
					() -> assertNull(test.get()),
					() -> test.set(""),
					() -> assertEquals("", test.get()),
					() -> test.set("test"),
					() -> assertEquals("test", test.get()),
					() -> assertThat(bundleClassLoader.getLoadedClassNames(), 
							containsInAnyOrder(Arrays.asList(TEST_INTERNAL_FACTORY_CLASS_NAME, TEST_INTERNAL_CLASS_NAME).toArray()))
					);
		}	
	}
	
	@ParameterizedTest
	@ValueSource(strings = {TEST_EXTERNAL_CLASS_NAME, TEST_INTERNAL_CLASS_NAME})
	void testCreateWithCustomConstructor(String className) throws IOException {
		loadTestResources();
		test.Test test = (test.Test) bundleClassLoader.create(className, new Object[] {"test"}, new Class[] {String.class});
		assertNotNull(test);
		if(className.equals(TEST_EXTERNAL_CLASS_NAME)) {
		assertAll("Test external implementation", 
				() -> assertNotNull(test),
				() -> assertEquals("test", test.get()),
				() -> test.set(""),
				() -> assertEquals("default", test.get()),
				() -> test.set("test"),
				() -> assertEquals("test", test.get())
				);
		} else {
		assertAll("Test internal implementation", 
				() -> assertNotNull(test),
				() -> assertEquals("test", test.get()),
				() -> test.set(""),
				() -> assertEquals("", test.get()),
				() -> test.set("test"),
				() -> assertEquals("test", test.get())
				);	
		}
		assertThat(bundleClassLoader.getLoadedClassNames(), containsInAnyOrder(Arrays.asList(className).toArray()));
	}
	
	@ParameterizedTest
	@ValueSource(strings = {TEST_EXTERNAL_FACTORY_CLASS_NAME, TEST_INTERNAL_FACTORY_CLASS_NAME})
	void testCreateWithCustomFactoryMethod(String factoryClassName) throws IOException {
		loadTestResources();
		test.Test test = (test.Test) bundleClassLoader.create(factoryClassName, "newInstance", new Object[] {"test"}, 
				new Class[] {String.class});
		assertNotNull(test);
		if(factoryClassName.equals(TEST_EXTERNAL_FACTORY_CLASS_NAME)) {
			assertAll("Test external implementation", 
					() -> assertNotNull(test),
					() -> assertEquals("test", test.get()),
					() -> test.set(""),
					() -> assertEquals("default", test.get()),
					() -> test.set("test"),
					() -> assertEquals("test", test.get()),
					() -> assertThat(bundleClassLoader.getLoadedClassNames(), 
							containsInAnyOrder(Arrays.asList(TEST_EXTERNAL_FACTORY_CLASS_NAME, TEST_EXTERNAL_CLASS_NAME).toArray()))
					);
		} else {
			assertAll("Test internal implementation", 
					() -> assertNotNull(test),
					() -> assertEquals("test", test.get()),
					() -> test.set(""),
					() -> assertEquals("", test.get()),
					() -> test.set("test"),
					() -> assertEquals("test", test.get()),
					() -> assertThat(bundleClassLoader.getLoadedClassNames(), 
							containsInAnyOrder(Arrays.asList(TEST_INTERNAL_FACTORY_CLASS_NAME, TEST_INTERNAL_CLASS_NAME).toArray()))
					);
		}	
	}
	
	@ParameterizedTest
	@ValueSource(strings = {TEST_INTERNAL_CLASS_NAME})
	void testCreateProxy(String className) throws IOException {
		bundleClassLoader.setAutoProxyEnabled(true);
		bundleClassLoader.addResources(Arrays.asList(testInternalJar));
		test.Test test = (test.Test) bundleClassLoader.create(className, new Object[] {"test"}, new Class[] {String.class});
		assertNotNull(test);
		assertAll("Test internal implementation", 
				() -> assertNotNull(test),
				() -> assertEquals("test", test.get()),
				() -> test.set(""),
				() -> assertEquals("", test.get()),
				() -> test.set("test"),
				() -> assertEquals("test", test.get()),
				() -> assertThat(bundleClassLoader.getLoadedClassNames(), 
						containsInAnyOrder(Arrays.asList(TEST_INTERNAL_CLASS_NAME).toArray()))
				);
	}
	
	@ParameterizedTest
	@ValueSource(strings = {TEST_ABSTRACT_CLASS_NAME})
	void testCreateProxyWithoutInterface(String className) throws IOException {
		ProxyProviderFactory.setDefaultProxyProvider(new CglibProxyProvider());
		bundleClassLoader.setAutoProxyEnabled(true);
		bundleClassLoader.addResources(Arrays.asList(testAbstractJar));
		test.AbstractTest test = (test.AbstractTest) bundleClassLoader.create(className, new Object[] {"test"}, new Class[] {String.class});
		assertNotNull(test);
		assertAll("Test abstract implementation", 
				() -> assertNotNull(test),
				() -> assertEquals("test", test.get()),
				() -> test.set(""),
				() -> assertEquals("", test.get()),
				() -> test.set("test"),
				() -> assertEquals("test", test.get()),
				() -> assertThat(bundleClassLoader.getLoadedClassNames(), 
						containsInAnyOrder(Arrays.asList(TEST_ABSTRACT_CLASS_NAME).toArray()))
				);
	}
	
	@Test
	void testGetLoadedClassNames() throws IOException {
		loadTestResources();
		assertTrue(bundleClassLoader.getLoadedClassNames().isEmpty());
		bundleClassLoader.create(TEST_INTERNAL_CLASS_NAME);
		bundleClassLoader.create(TEST_EXTERNAL_CLASS_NAME);
		assertThat(bundleClassLoader.getLoadedClassNames(), 
				containsInAnyOrder(Arrays.asList(TEST_INTERNAL_CLASS_NAME, TEST_EXTERNAL_CLASS_NAME).toArray()));
	}
	
	@Test
	void testGetLoadedResourceNames() throws IOException {
		assertTrue(bundleClassLoader.getLoadedResourceNames().isEmpty());
		loadTestResources();
		List<String> expectedNames = Stream.of(EXPECTED_LOADED_EXTERNAL_RESOURCES, EXPECTED_LOADED_INTERNAL_RESOURCES)
				.flatMap(x -> x.stream())
                .collect(Collectors.toList());
		assertThat(bundleClassLoader.getLoadedResourceNames(), containsInAnyOrder(expectedNames.toArray()));
	}

	@Test
	void testGetName() {
		assertEquals(NAME, bundleClassLoader.getName());
	}

	@Test
	void testUnloadAllClasses() throws IOException {
		loadTestResources();
		bundleClassLoader.create(TEST_INTERNAL_CLASS_NAME);
		bundleClassLoader.create(TEST_EXTERNAL_CLASS_NAME);
		assertThat(bundleClassLoader.getLoadedClassNames(), 
				containsInAnyOrder(Arrays.asList(TEST_INTERNAL_CLASS_NAME, TEST_EXTERNAL_CLASS_NAME).toArray()));
		bundleClassLoader.unloadAllClasses();
		assertTrue(bundleClassLoader.getLoadedClassNames().isEmpty());
	}

	@Test
	void testUnloadClass() throws IOException, ClassNotFoundException {
		loadTestResources();
		bundleClassLoader.create(TEST_INTERNAL_CLASS_NAME);
		bundleClassLoader.create(TEST_EXTERNAL_CLASS_NAME);
		assertThat(bundleClassLoader.getLoadedClassNames(), 
				containsInAnyOrder(Arrays.asList(TEST_INTERNAL_CLASS_NAME, TEST_EXTERNAL_CLASS_NAME).toArray()));
		bundleClassLoader.unloadClass(TEST_INTERNAL_CLASS_NAME);
		assertThat(bundleClassLoader.getLoadedClassNames(), 
				containsInAnyOrder(Arrays.asList(TEST_EXTERNAL_CLASS_NAME).toArray()));
	}
	
	private void loadTestResources() throws IOException {
		bundleClassLoader.addResources(Arrays.asList(testExternalJar, testInternalJar));
	}

}
