package test;

public abstract class AbstractTest {

	public abstract String get();
	
	public abstract void set(String value);
}
