package test;

public class TestPojo implements test.Test {

	String value;
	
	public TestPojo() {}

	@Override
	public String get() {
		return value;
	}

	@Override
	public void set(String value) {
		this.value = value;
	}
}
