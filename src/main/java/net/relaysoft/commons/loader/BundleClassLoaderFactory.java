package net.relaysoft.commons.loader;

/**
 * Bundle class loader factory offers convenience methods for creating and managing bundle class loader instances and creating new object 
 * instances with them.
 * 
 * @author relaysoft.net
 *
 */
public final class BundleClassLoaderFactory extends AbstractBundleClassLoaderFactory {
	
	private static BundleClassLoaderFactory bundleClassLoaderFactory = new BundleClassLoaderFactory();
	
	/**
	 * @return Default instance of bundle class loader factory.
	 */
	public static BundleClassLoaderFactory getInstance() {
		return bundleClassLoaderFactory;
	}
	
	private BundleClassLoaderFactory() {
		super();
	}
	
}
