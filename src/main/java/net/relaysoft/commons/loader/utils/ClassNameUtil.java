package net.relaysoft.commons.loader.utils;

import java.io.File;

public final class ClassNameUtil {
	
	private ClassNameUtil() {}

	/**
	 * Converts class <a href="https://docs.oracle.com/javase/specs/">binary name</a> to corresponding file path.
	 * 
	 * @param name - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class.
	 * @return Class file path as a file path string.
	 */
	public static String classNameToPathName(final String name) {
		String pathName = name != null ? name.replace(File.separatorChar, '~') : name;
		if(pathName != null && pathName.length() > 0) {
			pathName = pathName.replace('.', File.separatorChar).concat(".class");
			pathName = pathName.replace('~', File.separatorChar);
		}
		return pathName;
	}

}
