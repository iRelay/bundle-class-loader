package net.relaysoft.commons.loader.utils;

public final class StringUtil {
	
	/**
	 * Checks whether given string is either <code>null</code> or empty string. If string contains only white spaces, it is also 
	 * considered as blank.
	 * 
	 * @param string - String to check
	 * @return <code>true</code> if given string is <code>null</code> or empty.
	 */
	public static boolean isBlank(final String string) {
        int strLen;
        if (string == null || (strLen = string.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(string.charAt(i))) {
                return false;
            }
        }
        return true;
    }
	
	/**
	 * Checks whether given string is NOT <code>null</code> or empty string. If string contains only white spaces, it is considered as 
	 * blank.
	 * 
	 * @param string - String to check
	 * @return <code>true</code> if given string is NOT <code>null</code> or empty.
	 */
	public static boolean isNotBlank(final String string) {
        return !isBlank(string);
    }
	
	/**
	 * Convert string with wild card characters (<b>*</b> and <b>?</b>) to valid regular expression string.
	 * 
	 * @param string Which should be converted.
	 * @return Converted regular expression string.
	 */
	public static String toRegex(final String string) {
		StringBuilder buffer = new StringBuilder(string.length());
		buffer.append('^');
		for(int i = 0; i < string.length(); i++) {
			char c = string.charAt(i);
			switch(c) {
			case '*':
				buffer.append(".*");
				break;
			case '?':
				buffer.append(".");
				break;
			case '(':
			case ')':
			case '[':
			case ']':
			case '$':
			case '^':
			case '.':
			case '{':
			case '}':
			case '|':
			case '\\':
				buffer.append("\\");
				buffer.append(c);
				break;
			default:
				buffer.append(c);
				break;
			}
		}
		buffer.append('$');
		
		return buffer.toString();
	}
	
	private StringUtil() {}

}
