package net.relaysoft.commons.loader.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class StreamUtil {
	
	/**
	 * Copies input stream to byte array.
	 * 
	 * @param is - Input stream to copy from
	 * @return Input stream content as byte array.
	 * @throws IOException If copy operation fails.
	 */
	public static byte[] toByteArray(final InputStream is) throws IOException {
		try (final ByteArrayOutputStream os = new ByteArrayOutputStream()) {
			copy(is, os, new byte[4096]);
			return os.toByteArray();
		}
	}
	
	/**
	 * Copies given amount of bytes from input stream to byte array.
	 * 
	 * @param is - Input stream to copy from
	 * @param size - Size of bytes to copy. If negative value then whole input stream is copied.
	 * @return Copied input stream content as byte array.
	 * @throws IOException If copy operation fails.
	 */
	public static byte[] toByteArray(final InputStream is, final long size) throws IOException {
		if (size < 0) {
			return toByteArray(is);
		}
		if (size > Integer.MAX_VALUE) {
            throw new IllegalArgumentException(String.format("Size cannot be greater than Integer max value: %d", size));
        }
		if (size == 0) {
			return new byte[0];
		}
		final byte[] bytes = new byte[(int) size];
		int offset = 0;
		int read;
		while (offset < size && (read = is.read(bytes, offset, (int) (size - offset))) != -1) {
			offset += read;
		}
		if (offset != size) {
			throw new IOException(String.format("Unexpected read size. current: %d, expected: %d", offset, size));
		}
		return bytes;
	}

	private static long copy(final InputStream is, final OutputStream os, final byte[] buffer) throws IOException {
		long bytes = 0;
		int n;
		while ((n = is.read(buffer)) != -1) {
			os.write(buffer, 0, n);
			bytes += n;
		}
		return bytes;
	}

	private StreamUtil() {}

}
