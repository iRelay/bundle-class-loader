package net.relaysoft.commons.loader.utils;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.relaysoft.commons.loader.proxy.ProxyProviderFactory;

public final class ProxyUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(ProxyUtil.class);
	
	private ProxyUtil() {}
	
	/**
	 * Converts object into castable jdk proxy if object class implements any interfaces.
	 * 
	 * @param object - Object to convert.
	 * @return Proxy or input object.
	 */
	public static Object toCastableProxy(Object object) {
		return toCastableProxy(object, ProxyUtil.class.getClassLoader());
	}
	
	/**
	 * Converts object into castable jdk proxy if object class implements any interfaces.
	 * 
	 * @param object - Object to convert.
	 * @param classloader - Class loader to use for proxy creation.
	 * @return Proxy or input object.
	 */
	public static Object toCastableProxy(Object object, ClassLoader classloader) {
		Class<?>[] interfaces = getProxyObjectInterfaces(object);
		Class<?> superclass = getProxyObjectSuperclass(object);
		if(interfaces != null && interfaces.length > 0) {
			logger.trace("Creating proxy object from class [{}]", object.getClass().getName());
			return ProxyProviderFactory.create().createProxy(object, superclass, interfaces, classloader);
		} else if(superclass != null) {
			logger.trace("Creating proxy object from class [{}]", object.getClass().getName());
			return ProxyProviderFactory.create().createProxy(object, superclass, interfaces, classloader);
		}
		return object;
	}
	
	/**
	 * Get interfaces for the given object instance.
	 * 
	 * @param object - Object instance the check interfaces for
	 * @return Interfaces or empty array.
	 */
	public static Class<?>[] getProxyObjectInterfaces(Object object) {
		Class<?>[] interfaces = object.getClass().getInterfaces();
		if (logger.isTraceEnabled() && interfaces != null) {
			String interfaceNames = Arrays.stream(interfaces)
					.map(Class::getName)
					.collect(Collectors.joining(", ", "[", "]"));
			logger.trace("Found interfaces {} for the object class: {}.", interfaceNames, object.getClass().getName());
		}	
		return interfaces;
	}
	
	/**
	 * Get super class for the given object instance.
	 * 
	 * @param object - Object instance the check super class for
	 * @return Super class or <code>null</code>.
	 */
	public static Class<?> getProxyObjectSuperclass(Object object) {
		Class<?> superclass = object.getClass().getSuperclass();
		if (logger.isTraceEnabled() && superclass != null && !superclass.isInterface()) {
			logger.trace("Found super class {} for the object class: {}.", superclass, object.getClass().getName());
		}	
		return superclass != null && !superclass.isInterface() ? superclass : null;
	}
}
