package net.relaysoft.commons.loader.utils;

import java.io.File;

public final class FileNameUtil {
	
	private FileNameUtil() {}
	
	/**
	 * Resolves file extension name from file object.
	 * 
	 * @param file - File object from which extension should be resolved 
	 * @return Extension name without dot character.
	 */
	public static String getExtension(final File file) {
		if (file != null && file.exists()) {
			int index = indexOfExtension(file.getName());
			if (index == -1) {
				return "";
			} else {
				return file.getName().substring(index + 1);
			}
		}	
		return null;
	}

	private static int indexOfExtension(final String filename) {
		if (filename != null) {
			int extensionPos = filename.lastIndexOf('.');
			return indexOfLastSeparator(filename) > extensionPos ? -1 : extensionPos;
		}
		return -1;
	}

	private static int indexOfLastSeparator(final String filename) {
		if (filename != null) {
			return Math.max(filename.lastIndexOf('/'), filename.lastIndexOf('\\'));
		}
		return -1;
	}


}
