package net.relaysoft.commons.loader.utils;

import java.util.Arrays;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PropertyUtil {
	
	private static final String PROPERTY_FILE_NAME = "application.properties";

	private static Logger logger = LoggerFactory.getLogger(PropertyUtil.class);
	
	/**
	 * Load properties from <code>application.properties</code> file and system properties. System properties always override properties
	 * from file.
	 * 
	 * @return Loaded properties.
	 */
	public static Properties loadProperties(){
		Properties prop = new Properties();
		try {
			prop.load(PropertyUtil.class.getClassLoader().getResourceAsStream(PROPERTY_FILE_NAME));
		} catch (Exception e) {
			logger.info("{} does exists in classpath. Using only system properties.", PROPERTY_FILE_NAME);
		}
		Properties systemProperties = System.getProperties();
		for(String name : systemProperties.stringPropertyNames()){
			if(name.startsWith("relaysoft.bundle.loader")) {
				logger.debug("Setting property '{}' from system properties.", name);
				prop.setProperty(name, systemProperties.getProperty(name));
			}
		}
		return prop;
	}
	
	/**
	 * Get comma separated property value as string array.
	 * 
	 * @param properties - Properties to read from
	 * @param name - Property name to read value from
	 * @return String array of comma separated property values.
	 */
	public static String[] getAsStringArray(final Properties properties, final String name) {
		return Arrays.stream(properties.getProperty(name, "").split(","))
				.map(String::trim)
				.toArray(String[]::new);
	}
	
	private PropertyUtil() {}
}
