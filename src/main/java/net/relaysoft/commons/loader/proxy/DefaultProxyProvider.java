package net.relaysoft.commons.loader.proxy;

import java.lang.reflect.Proxy;

/**
 * Default proxy provider for bundle class loader.
 * 
 * @author relaysoft.net
 *
 */
public class DefaultProxyProvider implements ProxyProvider {
	
	DefaultProxyProvider() {}

	@Override
	public Object createProxy(Object object, Class<?> superClass, Class<?>[] interfaces, ClassLoader classloader) {
		ProxyInvocationHandler handler = new ProxyInvocationHandler(object);
        return Proxy.newProxyInstance(resolveClassLoader(classloader), interfaces, handler);
	}
	
	private static ClassLoader resolveClassLoader(ClassLoader classloader) {
		return classloader != null ? classloader : ProxyProviderFactory.class.getClassLoader();
	}
	
}
