package net.relaysoft.commons.loader.proxy;

/**
 * Proxy provider is used to provide instances of proxy classes.
 * 
 * @author relaysoft.net
 *
 */
public interface ProxyProvider {

	/**
	 * Returns an instance of a proxy class of the specified interfaces for given object.
	 * 
	 * @param object - Object from witch proxy class is invoked from
	 * @param superClass - Super class for the proxy class to extend
	 * @param interfaces - Interfaces for the proxy class to implement
	 * @param classloader - Class loader to define the proxy class
	 * @return Instance of a proxy class.
	 */
	Object createProxy(Object object, Class<?> superClass, Class<?>[] interfaces, ClassLoader classloader);
	
}
