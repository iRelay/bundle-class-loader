package net.relaysoft.commons.loader.proxy;

import java.lang.reflect.Method;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

public class CglibProxyProvider implements ProxyProvider {
	
	private class CglibProxyHandler implements MethodInterceptor {
		
        private final Object delegate;

        public CglibProxyHandler(Object delegate) {
            this.delegate = delegate;
        }

        @Override
        public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
            Method delegateMethod = delegate.getClass().getMethod( method.getName(), method.getParameterTypes() );
            return delegateMethod.invoke( delegate, args );
        }
    }

	@Override
	public Object createProxy(Object object, Class<?> superClass, Class<?>[] interfaces, ClassLoader classloader) {
		CglibProxyHandler handler = new CglibProxyHandler( object );
        return createEnhancer(object, superClass, interfaces, classloader, handler).create();
	}
	
	private static Enhancer createEnhancer(Object object, Class<?> superClass, Class<?>[] interfaces, ClassLoader classloader, 
			CglibProxyHandler handler) {
		Enhancer enhancer = new Enhancer();
		enhancer.setInterfaces(interfaces != null ? interfaces : new Class<?>[] {});
		enhancer.setSuperclass(superClass);
		enhancer.setClassLoader(resolveClassLoader(classloader));
		enhancer.setCallback(handler);	
		return enhancer;
	}
	
	private static ClassLoader resolveClassLoader(ClassLoader classloader) {
		return classloader != null ? classloader : ProxyProviderFactory.class.getClassLoader();
	}

}
