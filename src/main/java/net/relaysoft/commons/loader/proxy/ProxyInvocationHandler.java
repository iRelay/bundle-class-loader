package net.relaysoft.commons.loader.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Proxy object invocation handler.
 * 
 * @author relaysoft.net
 *
 */
class ProxyInvocationHandler implements InvocationHandler {
	
	private final Object object;

	ProxyInvocationHandler(Object object) {
		this.object = object;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		Method proxyInstance = object.getClass().getMethod(method.getName(), method.getParameterTypes());
        return proxyInstance.invoke(object, args);
	}

}
