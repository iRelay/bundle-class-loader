package net.relaysoft.commons.loader.proxy;

/**
 * Internal factory class for providing proxy provider instances for the bundle class loader in order to proxy loaded classes against 
 * interfaces.
 * 
 * @author relaysoft.net
 *
 */
public final class ProxyProviderFactory {
	
	private static ProxyProvider proxyProvider = new DefaultProxyProvider();

    /**
     * @return The instance of proxy provider implementation
     */
    public static ProxyProvider create() {
    	if(proxyProvider == null) {
    		ProxyProviderFactory.proxyProvider = new DefaultProxyProvider();
    	}
        return proxyProvider;
    }
    
    /**
	 * @param proxyProvider The default proxy provider implementation to use with this factory.
	 */
    public static void setDefaultProxyProvider(ProxyProvider proxyProvider) {
        ProxyProviderFactory.proxyProvider = proxyProvider;
    }

	private ProxyProviderFactory() {}

}
