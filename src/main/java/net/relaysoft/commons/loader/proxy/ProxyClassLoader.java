package net.relaysoft.commons.loader.proxy;

import java.io.InputStream;
import java.net.URL;

/**
 * Bundle proxy class loader interface. Proxy class loader forms the bundle class loader. 
 * 
 * @author relaysoft.net
 *
 */
public interface ProxyClassLoader {

	/**
	 * Loads the class with the specified <a href="https://docs.oracle.com/javase/specs/">binary name</a>. Instead of throwing 
	 * {@link ClassNotFoundException} if class is not found from this proxy loader it return <code>null</code>. 
	 * 
	 * @param name - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class
	 * @return Loaded class or <code>null</code>.
	 */
	public Class<?> loadClass(String name);
	
	/**
	 * Loads the class with the specified <a href="https://docs.oracle.com/javase/specs/">binary name</a>. Instead of throwing 
	 * {@link ClassNotFoundException} if class is not found from this proxy loader it return <code>null</code>. 
	 * 
	 * @param name - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class
	 * @param resolve - If <code>true</code> then resolve the class
	 * @return Loaded class or <code>null</code>.
	 */
	public Class<?> loadClass(String name, boolean resolve);

	/**
	 * Returns an input stream for reading the specified resource.
	 * 
	 * @param name - The resource name
	 * @return An input stream for reading the resource, or <code>null</code> if the resource could not be found.
	 */
	public InputStream getResourceAsStream(String name);

	/**
	 * Finds the resource with the given name.
	 * 
	 * @param name - The resource name
	 * @return URL object for reading the resource, or <code>null</code> if the resource could not be found.
	 */
	public URL getResource(String name);

}
