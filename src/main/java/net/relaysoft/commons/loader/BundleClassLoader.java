package net.relaysoft.commons.loader;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Bundle class loader is a special class loader which can bundle multiple class loaders in order to enable loading of classes from target 
 * JAR and CLASS sources at runtime.
 * <br><br>
 * It is possible to add new resources (JAR or CLASS) to bundle loader from {@link File}, {@link URL} or by directly passing resource 
 * content as byte array.
 * <br><br>
 * Bundle class loader will use the class loaders added into bundle in order which they were added. This enables changing the default class 
 * loader load order if needed.
 * 
 * @author relaysoft.net
 *
 */
public interface BundleClassLoader {

	/**
	 * Add new JAR or CLASS resource directly from byte content into this bundle class loader.
	 * 
	 * @param content - JAR or CLASS content in byte array
	 * @throws IOException If reading resource stream fails.
	 */
	void addResource(byte[] content) throws IOException;
	
	/**
	 * Add new JAR or CLASS resource from file into this bundle class loader.
	 * <br><br>
	 * Only files with <code>.jar</code> or <code>.class</code> extensions are supported.
	 * 
	 * @param file - JAR or CLASS file
	 * @throws IOException If reading resource stream fails.
	 */
	void addResource(File file) throws IOException;
	
	/**
	 * Add new JAR or CLASS resource loaded from given URL into this bundle class loader.
	 * 
	 * @param url - URL from which resource can be loaded from
	 * @throws IOException If reading resource stream fails.
	 */
	void addResource(URL url) throws IOException;
	
	/**
	 * Add new JAR or CLASS resources into this bundle class loader.
	 * <br><br>
	 * Accepted source types are:
	 * <ul>
	 * <li>{@link File}</li>
	 * <li>{@link URL}</li>
	 * <li>{@link byte[]}</li>
	 * </ul>
	 * Only files with <code>.jar</code> or <code>.class</code> extensions are supported.
	 * 
	 * @param sources - Resource source list
	 * @throws IOException If reading resource stream fails.
	 */
	void addResources(List<Object> sources) throws IOException;
	
	/**
	 * Creates the object instance of the specified class name by invoking the default constructor.
	 * <br><br>
	 * If object class implements any interface, then returned object instance is an instance of proxy class of the object which can be 
	 * cast to implemented interface. If object does not implement any interfaces it is returned as such.  
	 * 
	 * @param name - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class.
	 * @return Object instance of class.
	 */
	Object create(String name);
	
	/**
	 * Creates the object instance of the specified class name by invoking the the right arguments constructor.
	 * <br><br>
	 * If object class implements any interface, then returned object instance is an instance of proxy class of the object which can be 
	 * cast to implemented interface. If object does not implement any interfaces it is returned as such.  
	 * 
	 * @param name - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class.
	 * @param args - Arguments to the method call
	 * @return Object instance of class.
	 */
	Object create(String name, Object... args);
	
	/**
	 * Creates the object instance of the specified class name by invoking the the right arguments constructor based on the passed type 
	 * parameter array.
	 * <br><br>
	 * If object class implements any interface, then returned object instance is an instance of proxy class of the object which can be 
	 * cast to implemented interface. If object does not implement any interfaces it is returned as such. 
	 * 
	 * @param name - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class.
	 * @param args - Arguments to the method call
	 * @param types - Arguments types to the method call
	 * @return Object instance of class.
	 */
	Object create(String name, Object[] args, Class<?>[] types);
	
	/**
	 * Creates the object instance of the specified class name by invoking the the static factory method.
	 * <br><br>
	 * If object class implements any interface, then returned object instance is an instance of proxy class of the object which can be 
	 * cast to implemented interface. If object does not implement any interfaces it is returned as such. 
	 * 
	 * @param name - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class.
	 * @param methodName - Name of the factory method.
	 * @param args - Arguments to the method call
	 * @return Object instance of class.
	 */
	Object create(String name, String methodName, Object... args);
	
	/**
	 * Creates the object instance of the specified class name by invoking the the static factory method.
	 * <br><br>
	 * If object class implements any interface, then returned object instance is an instance of proxy class of the object which can be 
	 * cast to implemented interface. If object does not implement any interfaces it is returned as such. 
	 * 
	 * @param name - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class.
	 * @param methodName - Name of the factory method.
	 * @param args - Arguments to the method call
	 * @param types - Arguments types to the method call
	 * @return Object instance of class.
	 */
	Object create(String name, String methodName, Object[] args, Class<?>[] types);
	
	/**
	 * @return Name of all classes loaded locally (NOT by Java build-in class loaders) by this bundle class loader.
	 */
	List<String> getLoadedClassNames();
	
	/**
	 * @return Name of all resources added into this bundle class loader.
	 */
	List<String> getLoadedResourceNames();	
	
	/**
	 * @return Name of this bundle class loader.
	 */
	String getName();
	
	/**
	 * @return <code>true</code> if bundle class loader automatically create proxy objects for object instances implementing interfaces.
	 */
	boolean isAutoProxyEnabled();
	
	/**
	 * Enable or disable auto proxy.
	 * 
	 * @param enable <code>true</code> or <code>false</code>
	 */
	void setAutoProxyEnabled(boolean enable);
	
	/**
	 * Removes all loaded classes and resources.
	 */
	void unloadAll();
	
	/**
	 * Unload all classes loaded by this bundle class loader.
	 */
	void unloadAllClasses();
	
	/**
	 * Unload specific class loaded by this bundle class loader.
	 * 
	 * @param name - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class.
	 */
	void unloadClass(String name);
	
	/**
	 * Removes the loaded resource
	 * 
	 * @param name - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the resource
	 */
	void unloadResource(String name);
	
	
}
