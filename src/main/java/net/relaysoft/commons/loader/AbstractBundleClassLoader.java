package net.relaysoft.commons.loader;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.relaysoft.commons.loader.proxy.ProxyClassLoader;
import net.relaysoft.commons.loader.proxy.ProxyProviderFactory;
import net.relaysoft.commons.loader.utils.FileNameUtil;
import net.relaysoft.commons.loader.utils.ProxyUtil;
import net.relaysoft.commons.loader.utils.StreamUtil;
import net.relaysoft.commons.loader.utils.StringUtil;

/**
 * Abstract bundle class loader implementation.
 * 
 * @author relaysoft.net
 *
 */
public abstract class AbstractBundleClassLoader extends ClassLoader implements BundleClassLoader {

	private static final String CLASS_EXTENSION = "class";
	private static final String JAR_EXTENSION = "jar";

	protected final Logger logger;
	
	private final String name;

	private final Map<String, ResourceEntry> loadedResources;

	private final Set<ProxyClassLoader> proxyClassloaders;

	/**
	 * Creates a new service bundle loader.
	 * 
	 * @param name - Bundle class loader name
	 */
	protected AbstractBundleClassLoader(String name) {
		this(name, null);
	}

	/**
	 * Creates a new service bundle loader using the specified parent class loader for delegation.
	 * 
	 * @param name - Bundle class loader name
	 * @param parent - The optional parent class loader to use
	 */
	protected AbstractBundleClassLoader(String name, ClassLoader parent) {
		super(resolveParentClassLoader(parent));
		this.name = name;
		proxyClassloaders = new LinkedHashSet<>();
		loadedResources = Collections.synchronizedMap(new HashMap<String, ResourceEntry>());
		logger = LoggerFactory.getLogger(getClass());
	}
	
	@Override
	public List<String> getLoadedResourceNames() {
		return new ArrayList<>(loadedResources.keySet());
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public URL getResource(String name) {
		URL url = null;
		if (StringUtil.isNotBlank(name)){
			for (ProxyClassLoader loader : proxyClassloaders) {
				url = loader.getResource(name);
				if (url != null) {
					logger.debug("name={}, proxyLoader={}, message=Loaded resource: [{}]", getName(), loader.getClass().getSimpleName(), 
							name);
					break;
				}
			}
		}
		return url;
	}

	@Override
	public InputStream getResourceAsStream(String name) {
		InputStream is = null;
		if (StringUtil.isNotBlank(name)){
			for (ProxyClassLoader loader : proxyClassloaders) {
				is = loader.getResourceAsStream(name);
				if (is != null) {
					logger.debug("name={}, proxyLoader={}, message=Loaded resource [{}] as stream.", getName(), 
							loader.getClass().getSimpleName(), name);
					break;
				}
			}
		}
		return is;
	}

	@Override
	public Class<?> loadClass(String name) throws ClassNotFoundException {
		return (loadClass(name, true));
	}

	@Override
	public Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
		Class<?> clazz = null;
		if (StringUtil.isNotBlank(name)){
			for (ProxyClassLoader loader : proxyClassloaders) {
				logger.trace("name={}, proxyLoader={}, message=Trying to load class: [{}]", getName(), loader.getClass().getSimpleName(), 
						name);
				clazz = loader.loadClass(name, resolve);
				if (clazz != null) {
					logger.debug("name={}, proxyLoader={}, message=Loaded class: [{}]", getName(), loader.getClass().getSimpleName(), 
							clazz.getName());
					break;
				}
			}
			if (clazz == null){
				throw new ClassNotFoundException(name);
			}
		}
		return clazz;
	}
	
	@Override
	public void unloadAll() {
		logger.debug("name={}, message=Unloading all resources from bundle class loader.", getName());
		unloadAllClasses();
		getLoadedResourceNames().stream().forEach(this::unloadResource);
	}
	
	@Override
	public void unloadResource(final String name) {	
		if (loadedResources.containsKey(name)) {
			logger.debug("name={}, message=Unloading resource {}.", this.name, name);
			loadedResources.remove(name);
		} else {
			logger.warn("name={}, message=Cannot unload resource entry '{}' because it does not exists!", this.name, name);
		}
	}

	/**
	 * Add new {@link ProxyClassLoader} to this bundle class loader.
	 * 
	 * @param loader - New {@link ProxyClassLoader} to add.
	 */
	protected void addProxyClassLoader(final ProxyClassLoader loader) {
		if(proxyClassloaders.add(loader)) {
			logger.debug("name={}, message=Added new proxy class loader: [{}].", name, loader.getClass().getSimpleName());
		}
	}
	
	/**
	 * Creates an instance of proxy class form given object if possible. This requires object class to implement at least one interface.
	 * 
	 * @param object The object from which proxy should be generated.
	 * @return Instance of proxy class or given object if cannot be used as proxy instance.
	 */
	protected Object createProxyObject(Object object) {
		Class<?>[] interfaces = getProxyObjectInterfaces(object);
		Class<?> superclass = getProxyObjectSuperclass(object);
		if(interfaces != null && interfaces.length > 0) {
			logger.trace("name={}, message=Creating new proxy object implementing interfaces {} from class [{}] object instance", 
					name, interfaces, object.getClass().getName());
			return ProxyProviderFactory.create().createProxy(object, superclass, interfaces, this);
		} else if(superclass != null) {
			logger.trace("name={}, message=Creating new proxy object extending super class {} from class [{}] object instance", 
					name, superclass, object.getClass().getName());
			return ProxyProviderFactory.create().createProxy(object, superclass, interfaces, this);
		}
		return object;
	}

	/**
	 * Get byte content of the loaded resource. 
	 * 
	 * @param name - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the resource
	 * @return Resource content.
	 */
	protected byte[] getResourceContent(final String name) {
		return loadedResources.containsKey(name) ? loadedResources.get(name).getContent() : null;
	}

	/**
	 * Load new resource from byte source.
	 * 
	 * @param content - Resource content bytes
	 * @throws IOException If reading the resource content stream fails. 
	 */
	protected void loadResourceFromBytes(final byte[] content) throws IOException {
		try(InputStream is = new ByteArrayInputStream(content)) {
			loadJar(is);
		}
	}

	/**
	 * Load new resource from URL. Can point to local or remote JAR or CLASS resource.
	 * 
	 * @param url - URL to the resource content
	 * @throws IOException If reading the resource content stream fails. 
	 */
	protected void loadResourceFromUrl(final URL url) throws IOException {
		try(InputStream is = url.openStream()) {
			loadJar(is, url);
		}
	}

	/**
	 * Load new resource from file. Can be JAR or CLASS file. Also loads all JAR and CLASS files from given directory.
	 * 
	 * @param file - Resource file to load.
	 * @throws IOException If reading the resource content stream fails. 
	 */
	protected void loadResourceFromFile(final File file) throws IOException {
		if(file != null && file.exists()) {
			if(isJarFile(file) || isClassFile(file)) {
				try(FileInputStream fis = new FileInputStream(file)) {
					loadJar(fis, file.toURI().toURL());
				}
			} else if(file.isDirectory()) {
				String[] subFileNames = file.list();
				if(subFileNames != null) {
					String currentFilePath = file.getAbsolutePath();
					for (String subFileName : subFileNames) {
						File subFile = new File(currentFilePath.concat(File.separator).concat(subFileName));
						loadResourceFromFile(subFile);
					}
				}
			} else {
				logger.trace("name={}, message=Skip loading of file {}. It is not JAR or CLASS file.", name, file.getName());
			}
		}
	}
	
	private Class<?>[] getProxyObjectInterfaces(Object object) {
		Class<?>[] interfaces = ProxyUtil.getProxyObjectInterfaces(object);
		if (logger.isTraceEnabled() && interfaces != null) {
			String interfaceNames = Arrays.stream(interfaces)
					.map(Class::getName)
					.collect(Collectors.joining(", ", "[", "]"));
			logger.trace("name={}, message=Found interfaces {} for created class: {}.", name, interfaceNames, object.getClass().getName());
		}	
		return interfaces;
	}
	
	private Class<?> getProxyObjectSuperclass(Object object) {
		Class<?> superclass = ProxyUtil.getProxyObjectSuperclass(object);
		if (logger.isTraceEnabled() && superclass != null && !superclass.isInterface()) {
			logger.trace("name={}, message=Found super class {} for created class: {}.", name, superclass, object.getClass().getName());
		}	
		return superclass != null && !superclass.isInterface() ? superclass : null;
	}
	
	private boolean isClassFile(final File file) {
		return file.isFile() && FileNameUtil.getExtension(file).equals(CLASS_EXTENSION);
	}

	private boolean isJarFile(final File file) {
		return file.isFile() && FileNameUtil.getExtension(file).equals(JAR_EXTENSION);
	}

	private void loadJar(final InputStream is) throws IOException {
		loadJar(is, null);
	}

	private void loadJar(final InputStream is, final URL url) throws IOException {
		try(JarInputStream jis = new JarInputStream(new BufferedInputStream(is))) {
			JarEntry jarEntry = null;
			while ((jarEntry = jis.getNextJarEntry()) != null) {
				loadJarEntry(jarEntry, jis, url);
			}
		}
	}

	private void loadJarEntry(final JarEntry jarEntry, final JarInputStream jis, final URL url) throws IOException {
		String entryName = jarEntry.getName();
		boolean isFileEntry = !jarEntry.isDirectory();
		if(isFileEntry && !loadedResources.containsKey(entryName)) {
			long size = jarEntry.getSize();
			if(logger.isTraceEnabled()) {
				logger.trace("name={}, message=Loading JAR entry, entry={}, size={}, compression={}.", name, entryName,
						(size > 0 ? String.format("%d bytes", size) : "unknown"),
						(jarEntry.getMethod() == ZipEntry.DEFLATED ? "compressed" : "uncompressed"));
			}
			byte[] content = StreamUtil.toByteArray(jis, size);
			if(content.length > 0) {
				ResourceEntry jarResourceEntry = new ResourceEntry.Builder(entryName, content).url(url).size(size).build();
				loadedResources.put(entryName, jarResourceEntry);
			} else {
				logger.warn("name={}, message=JAR entry [{}] was empty, url={}.", name, entryName, url);
			}
		} 
	}
	
	private static ClassLoader resolveParentClassLoader(ClassLoader parent) {
		if(parent == null) {
			ClassLoader loader = AbstractBundleClassLoader.class.getClassLoader();
			return loader.getParent() != null ? loader.getParent() : loader;
		}
		return parent;
	}

}
