package net.relaysoft.commons.loader;

import java.net.URL;

public class ResourceEntry {
	
	public static class Builder {
		
		private String name;
		
		private long size;
		
		private URL url;
		
		private byte[] content;
		
		public Builder(String name, byte[] content) {
			this.name = name;
			this.content = content;
		}
			
		public ResourceEntry build() {
			ResourceEntry entry = new ResourceEntry();
			entry.name = this.name;
			entry.content = this.content;
			entry.size = this.size;
			entry.url = this.url;
			return entry;
		}
		
		public Builder size(long size) {
			this.size = size;
			return this;
		}
		
		public Builder url(URL url) {
			this.url = url;
			return this;
		}
		
	}
	
	private String name;
	
	private long size;
	
	private URL url;
	
	private byte[] content;
	
	public String getName() {
		return name;
	}
	
	public long getSize() {
		return size;
	}

	public URL getUrl() {
		return url;
	}

	public byte[] getContent() {
		return content;
	}
	
	private ResourceEntry() {}

}
