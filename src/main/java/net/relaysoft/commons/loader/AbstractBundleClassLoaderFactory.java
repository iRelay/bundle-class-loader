package net.relaysoft.commons.loader;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Abstract bundle class loader factory for common implementations for the bundle class loader factory implementations.
 * 
 * @author relaysoft.net
 *
 */
public abstract class AbstractBundleClassLoaderFactory {
	
	private final Map<String, BundleClassLoader> bundleClassLoaders;
	
	/**
	 * Default constructor for abstract bundle class loader factory. 
	 */
	protected AbstractBundleClassLoaderFactory() {
		bundleClassLoaders = Collections.synchronizedMap(new HashMap<>());
	}
	
	/**
	 * Add new JAR or CLASS resource directly from byte content into named bundle class loader.
	 * <br><br>
	 * Note: If bundle class loader with given name does not exist, then new bundle class loader is automatically created with default 
	 * configurations.
	 * 
	 * @param name - Bundle class loader name
	 * @param content - JAR or CLASS content in byte array
	 * @throws IOException If reading resource stream fails.
	 */
	public void addResourceToBundleClassLoader(String name, byte[] content) throws IOException {
		getBundleClassLoader(name).addResource(content);
	}
	
	/**
	 * Add new JAR or CLASS resource from file into this bundle class loader.
	 * <br><br>
	 * Only files with <code>.jar</code> or <code>.class</code> extensions are supported.
	 * <br><br>
	 * Note: If bundle class loader with given name does not exist, then new bundle class loader is automatically created with default 
	 * configurations.
	 * 
	 * @param name - Bundle class loader name
	 * @param file - JAR or CLASS file
	 * @throws IOException If reading resource stream fails.
	 */
	public void addResourceToBundleClassLoader(String name, File file) throws IOException {
		getBundleClassLoader(name).addResource(file);
	}		
	
	/**
	 * Add new JAR or CLASS resource loaded from given URL into this bundle class loader.
	 * <br><br>
	 * Note: If bundle class loader with given name does not exist, then new bundle class loader is automatically created with default 
	 * configurations.
	 * 
	 * @param name - Bundle class loader name
	 * @param url - URL from which resource can be loaded from
	 * @throws IOException If reading resource stream fails.
	 */
	public void addResourceToBundleClassLoader(String name, URL url) throws IOException {
		getBundleClassLoader(name).addResource(url);
	}
	
	/**
	 * Add new JAR or CLASS resources into this bundle class loader.
	 * <br><br>
	 * Accepted source types are:
	 * <ul>
	 * <li>{@link File}</li>
	 * <li>{@link URL}</li>
	 * <li>{@link byte[]}</li>
	 * </ul>
	 * Only files with <code>.jar</code> or <code>.class</code> extensions are supported.
	 * <br><br>
	 * Note: If bundle class loader with given name does not exist, then new bundle class loader is automatically created with default 
	 * configurations.
	 * 
	 * @param name - Bundle class loader name
	 * @param sources - Resource source list
	 * @throws IOException If reading resource stream fails.
	 */
	public void addResourcesToBundleClassLoader(String name, List<Object> sources) throws IOException {
		getBundleClassLoader(name).addResources(sources);
	}
	
	/**
	 * Creates the object instance of the specified class name with named bundle class loader by invoking the default constructor.
	 * <br><br>
	 * If object class implements any interface, then returned object instance is an instance of proxy class of the object which can be 
	 * cast to implemented interface. If object does not implement any interfaces it is returned as such.
	 * <br><br>
	 * Note: If bundle class loader with given name does not exist, then new bundle class loader is automatically created with default 
	 * configurations.
	 * 
	 * @param name - Bundle class loader name
	 * @param className - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class.
	 * @return Object instance of class.
	 */
	public Object createObject(String name, String className) {
		return getBundleClassLoader(name).create(className);
	}
	
	/**
	 * Creates the object instance of the specified class name with named bundle class loader by invoking the the right arguments 
	 * constructor.
	 * <br><br>
	 * If object class implements any interface, then returned object instance is an instance of proxy class of the object which can be 
	 * cast to implemented interface. If object does not implement any interfaces it is returned as such.
	 * <br><br>
	 * Note: If bundle class loader with given name does not exist, then new bundle class loader is automatically created with default 
	 * configurations.
	 * 
	 * @param name - Bundle class loader name
	 * @param className - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class.
	 * @param args - Arguments to the method call
	 * @return Object instance of class.
	 */
	public Object createObject(String name, String className, Object... args) {
		return getBundleClassLoader(name).create(className, args);
	}
	
	/**
	 * Creates the object instance of the specified class name with named bundle class loader by invoking the the right arguments 
	 * constructor based on the passed type parameter array.
	 * <br><br>
	 * If object class implements any interface, then returned object instance is an instance of proxy class of the object which can be 
	 * cast to implemented interface. If object does not implement any interfaces it is returned as such.
	 * <br><br>
	 * Note: If bundle class loader with given name does not exist, then new bundle class loader is automatically created with default 
	 * configurations.
	 * 
	 * @param name - Bundle class loader name
	 * @param className - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class.
	 * @param args - Arguments to the method call
	 * @param types - Arguments types to the method call
	 * @return Object instance of class.
	 */
	public Object createObject(String name, String className, Object[] args, Class<?>[] types) {
		return getBundleClassLoader(name).create(className, args, types);
	}
	
	/**
	 * Creates the object instance of the specified class name with named bundle class loader by invoking the the static factory method.
	 * <br><br>
	 * If object class implements any interface, then returned object instance is an instance of proxy class of the object which can be 
	 * cast to implemented interface. If object does not implement any interfaces it is returned as such.
	 * <br><br>
	 * Note: If bundle class loader with given name does not exist, then new bundle class loader is automatically created with default 
	 * configurations.
	 * 
	 * @param name - Bundle class loader name
	 * @param className - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class.
	 * @param methodName - Name of the factory method.
	 * @param args - Arguments to the method call
	 * @return Object instance of class.
	 */
	public Object createObject(String name, String className, String methodName, Object... args) {
		return getBundleClassLoader(name).create(className, methodName, args);
	}
	
	/**
	 * Creates the object instance of the specified class name with named bundle class loader by invoking the the static factory method.
	 * <br><br>
	 * If object class implements any interface, then returned object instance is an instance of proxy class of the object which can be 
	 * cast to implemented interface. If object does not implement any interfaces it is returned as such.
	 * <br><br>
	 * Note: If bundle class loader with given name does not exist, then new bundle class loader is automatically created with default 
	 * configurations.
	 * 
	 * @param name - Bundle class loader name
	 * @param className - The <a href="https://docs.oracle.com/javase/specs/">binary name</a> of the class.
	 * @param methodName - Name of the factory method.
	 * @param args - Arguments to the method call
	 * @param types - Arguments types to the method call
	 * @return Object instance of class.
	 */
	public Object createObject(String name, String className, String methodName, Object[] args, Class<?>[] types) {
		return getBundleClassLoader(name).create(className, methodName, args, types);
	}
	
	/**
	 * Get bundle class loader instance with the given name.
	 * <br><br>
	 * Note: If bundle class loader with given name does not exist, then new bundle class loader is automatically created with default 
	 * configurations.
	 * 
	 * @param name - Bundle class loader name
	 * @return Bundle class loader instance.
	 */
	public BundleClassLoader getBundleClassLoader(String name) {
		if(!bundleClassLoaders.containsKey(name)) {
			bundleClassLoaders.put(name, initializeBundleClassloader(name));
		}	
		return bundleClassLoaders.get(name);
	}
	
	/**
	 * @return Names for all bundle class loaders initialized into this factory instance.
	 */
	public List<String> getBundleClassLoaderNames() {
		return bundleClassLoaders.keySet().stream().collect(Collectors.toList());
	}
	
	/**
	 * Initialize new or existing bundle class loader for this factory instance. If bundle class loader instance with given name already 
	 * exists within the factory, then it is re-initialized. All previously loaded resources are removed and classes unloaded.
	 * 
	 * @param name - Bundle class loader name
	 * @return Initialized bundle class loader instance.
	 */
	public BundleClassLoader initializeBundleClassloader(String name) {		
		return initializeBundleClassloader(name, null, null, new String[0]);
	}
	
	/**
	 * Initialize new or existing bundle class loader for this factory instance. If bundle class loader instance with given name already 
	 * exists within the factory, then it is re-initialized. All previously loaded resources are removed and classes unloaded.
	 * 
	 * @param name - Bundle class loader name
	 * @param bootPackages - The optional boot loader packages
	 * @return Initialized bundle class loader instance.
	 */
	public BundleClassLoader initializeBundleClassloader(String name, String... bootPackages) {		
		return initializeBundleClassloader(name, null, null, bootPackages);
	}
	
	/**
	 * Initialize new or existing bundle class loader for this factory instance. If bundle class loader instance with given name already 
	 * exists within the factory, then it is re-initialized. All previously loaded resources are removed and classes unloaded.
	 * 
	 * @param name - Bundle class loader name
	 * @param parent - The optional parent class loader to use
	 * @return Initialized bundle class loader instance.
	 */
	public BundleClassLoader initializeBundleClassloader(String name, ClassLoader parent) {
		return initializeBundleClassloader(name, parent, null, new String[0]);
	}
	
	/**
	 * Initialize new or existing bundle class loader for this factory instance. If bundle class loader instance with given name already 
	 * exists within the factory, then it is re-initialized. All previously loaded resources are removed and classes unloaded.
	 * 
	 * @param name - Bundle class loader name
	 * @param parent - The optional parent class loader to use
	 * @param bootPackages - The optional boot loader packages
	 * @return Initialized bundle class loader instance.
	 */
	public BundleClassLoader initializeBundleClassloader(String name, ClassLoader parent, String... bootPackages) {
		return initializeBundleClassloader(name, parent, null, bootPackages);
	}
	
	/**
	 * Initialize new or existing bundle class loader for this factory instance. If bundle class loader instance with given name already 
	 * exists within the factory, then it is re-initialized. All previously loaded resources are removed and classes unloaded.
	 * 
	 * @param name - Bundle class loader name
	 * @param properties - The optional configuration properties
	 * @return Initialized bundle class loader instance.
	 */
	public BundleClassLoader initializeBundleClassloader(String name, Properties properties) {		
		return initializeBundleClassloader(name, null, properties);
	}
	
	/**
	 * Initialize new or existing bundle class loader for this factory instance. If bundle class loader instance with given name already 
	 * exists within the factory, then it is re-initialized. All previously loaded resources are removed and classes unloaded.
	 * 
	 * @param name - Bundle class loader name
	 * @param properties - The optional configuration properties
	 * @param bootPackages - The optional boot loader packages
	 * @return Initialized bundle class loader instance.
	 */
	public BundleClassLoader initializeBundleClassloader(String name, Properties properties, String... bootPackages) {		
		return initializeBundleClassloader(name, null, properties, bootPackages);
	}
	
	/**
	 * Initialize new or existing bundle class loader for this factory instance. If bundle class loader instance with given name already 
	 * exists within the factory, then it is re-initialized. All previously loaded resources are removed and classes unloaded.
	 * 
	 * @param name - Bundle class loader name
	 * @param parent - The optional parent class loader to use
	 * @param properties - The optional configuration properties
	 * @return Initialized bundle class loader instance.
	 */
	public BundleClassLoader initializeBundleClassloader(String name, ClassLoader parent, Properties properties) {
		return initializeBundleClassloader(name, parent, properties, new String[0]);
	}

	/**
	 * Initialize new or existing bundle class loader for this factory instance. If bundle class loader instance with given name already 
	 * exists within the factory, then it is re-initialized. All previously loaded resources are removed and classes unloaded.
	 * 
	 * @param name - Bundle class loader name
	 * @param parent - The optional parent class loader to use
	 * @param properties - The optional configuration properties
	 * @param bootPackages - The optional boot loader packages
	 * @return Initialized bundle class loader instance.
	 */
	public BundleClassLoader initializeBundleClassloader(String name, ClassLoader parent, Properties properties, 
			String... bootPackages) {
		BundleClassLoader previousLoader = bundleClassLoaders.put(name, createNewBundleClassLoader(name, parent, properties, bootPackages));
		if(previousLoader != null) {
			previousLoader.unloadAllClasses();
		}
		return bundleClassLoaders.get(name);
	}
	
	/**
	 * Removes bundle class loader with given name from this factory instance. Unloads all the classes loaded with removed bundle class 
	 * loader. If bundle class loader with given name does not exists, then this method does not have any affect.
	 *  
	 * @param name - Bundle class loader name
	 */
	public void terminateBundleClassloader(String name) {
		BundleClassLoader loader = bundleClassLoaders.remove(name);
		if(loader != null) {
			loader.unloadAll();
		}
	}
	
	/**
	 * Removes all bundle class loaders from this factory instance. Unloads all the classes loaded with bundle class loaders.
	 */
	public void terminateBundleClassloaders() {
		for(BundleClassLoader loader : bundleClassLoaders.values()) {
			loader.unloadAll();
		}
		bundleClassLoaders.clear();
	}
	
	private BundleClassLoader createNewBundleClassLoader(String name, ClassLoader parent, Properties properties, String... bootPackages) {
		return new BundleClassLoaderImpl(name, parent, properties, bootPackages);
	}

}
