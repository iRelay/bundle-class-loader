package net.relaysoft.commons.loader.exceptions;

/**
 * Proxy loader class not found exception.
 * 
 * @author relaysoft.net
 *
 */
public class ProxyClassNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	/**
	 * New proxy class not found exception.
	 * 
	 * @param message - Error message
	 */
	public ProxyClassNotFoundException(String message) {
		super(message);
	}

	/**
	 * New proxy class not found exception.
	 * 
	 * @param message - Error message
	 * @param cause - Error cause
	 */
	public ProxyClassNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
