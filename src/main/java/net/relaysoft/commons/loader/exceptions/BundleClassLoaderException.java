package net.relaysoft.commons.loader.exceptions;

/**
 * Generic bundle class loader exception.
 * 
 * @author relaysoft.net
 *
 */
public class BundleClassLoaderException extends RuntimeException {

	/**
	 * Default serial id
	 */
	private static final long serialVersionUID = 1L;


	/**
	 * New generic bundle class loader exception.
	 * 
	 * @param message - Error message
	 */
	public BundleClassLoaderException(String message) {
		super( message );
	}

	/**
	 * New generic bundle class loader exception.
	 * 
	 * @param message - Error message
	 * @param cause - Error cause
	 */
	public BundleClassLoaderException(String message, Throwable cause) {
		super( message, cause );
	}

}
