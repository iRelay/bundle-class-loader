package net.relaysoft.commons.loader;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.relaysoft.commons.loader.exceptions.BundleClassLoaderException;
import net.relaysoft.commons.loader.exceptions.ProxyClassNotFoundException;
import net.relaysoft.commons.loader.proxy.ProxyClassLoader;
import net.relaysoft.commons.loader.utils.ClassNameUtil;
import net.relaysoft.commons.loader.utils.PropertyUtil;
import net.relaysoft.commons.loader.utils.StringUtil;

/**
 * Default bundle class loader implementation.
 * <br><br>
 * Automatically initializes following {@link ProxyClassLoader proxy class loaders}:
 * <ul>
 * <li><i>Boot</i>: This proxy loader mimics bootstrap class loader by handling the loading of needed classes from <i>java.*</i> 
 * package. This overrides all other proxy class loaders.</li>
 * <li><ul> 
 * <li>This class loader can be configured to handle additional class packages through application/system property 
 * <i>relaysoft.bundle.loader.boot.packages</i> by providing comma separated list of packages or package wildcards.</li>
 * </ul></li>
 * <li><i>Local</i>: This is bundle class loader's own internal proxy class loader used for loading all resources added to the bundle, 
 * unless class package is overridden by <i>Boot</i> loader.</li>
 * <li><ul>
 * <li>All the following proxy class loaders will handle the loading of resources not found by the <i>Local</i> loader.</li>
 * </ul></li>
 * <li><i>Current</i>: Proxy class loader which holds the class loader used to load bundle loader.</li>
 * <li><i>Parent</i>: Proxy class loader which by default holds the parent loader for the class loader which loaded the bundle loader.</li>
 * <li><ul>
 * <li>Parent loader used inside proxy loader can be overridden with bundle class loader constructor if needed.</li>
 * </ul></li>
 * <li><i>Thread context</i>: Proxy class loader which holds the class loader for current thread.</li>
 * <li><i>System</i>: Proxy class loader which holds the system class loader.</li>
 * </ul>
 * 
 * @author relaysoft.net
 *
 */
public class BundleClassLoaderImpl extends AbstractBundleClassLoader {
	
	private final class BootClassLoader implements ProxyClassLoader {

		private static final String JAVA_PACKAGE = "java.";
		
		private final String[] bootPackages;
		
		public BootClassLoader(String[] bootPackages) {
			this.bootPackages = bootPackages != null && bootPackages.length > 0 ? bootPackages : 
				PropertyUtil.getAsStringArray(properties, "relaysoft.bundle.loader.boot.packages");
		}

		@Override
		public URL getResource(String name) {
			URL url = null;
			if (isInBootPackage(name)) {
				url = parentLoader.getResource(name);
				if (url == null) {
					throw new ProxyClassNotFoundException(String.format("Resource [%s] not found.", name));
				} 
			}
			return url;
		}
		
		@Override
		public Class<?> loadClass(String name) {
			return loadClass(name, true);
		}

		@Override
		public Class<?> loadClass(String name, boolean resolve) {
			Class<?> clazz = null;
			if (isInBootPackage(name)) {
				clazz = parentLoader.loadClass(name, resolve);
				if(clazz == null){
					throw new ProxyClassNotFoundException(String.format("Class [%s] not found.", name));
				}
			}
			return clazz;
		}

		@Override
		public InputStream getResourceAsStream(String name) {
			InputStream is = null;
			if (isInBootPackage(name)) {
				is = parentLoader.getResourceAsStream(name);
				if (is == null) {
					throw new ProxyClassNotFoundException(String.format("Resource [%s] not found.", name));
				}
			}
			return is;
		}

		/**
		 * Check if the class/resource is part of boot defined boot packages.
		 * 
		 * @param name - Resource or class binary name 
		 * @return <code>true</code> if class or resource is defined as boot package.
		 */
		private boolean isInBootPackage(String name) {
			if(name.startsWith(JAVA_PACKAGE)){
				return true;
			}
			if(bootPackages != null) {
				for(String bootPackage : bootPackages) {
					Pattern pattern = 
							Pattern.compile(StringUtil.toRegex(bootPackage), Pattern.CASE_INSENSITIVE);
					Matcher matcher = pattern.matcher(name);
					if (matcher.find()) {
						return true;
					}
				}
			}
			return false;
		}

	}
	
	private final class LocalClassLoader implements ProxyClassLoader {

		@Override
		public URL getResource(String name) {		
			return getResource(name);
		}
		
		@Override
		public Class<?> loadClass(String name) {
			return loadClass(name, true);
		}

		@Override
		public Class<?> loadClass(String name, boolean resolve) {
			Class<?> clazz = loadedClasses.get(name);
			if(clazz == null){
				byte[] classBytes = loadClassBytes(name);
				if (classBytes != null) {
					clazz = defineClass(name, classBytes, 0, classBytes.length);
					if (clazz.getPackage() == null) {
						String packageName = (name.lastIndexOf('.') >= 0) ? name.substring(0, name.lastIndexOf('.')) : "";
						definePackage(packageName, null, null, null, null, null, null, null);
					}
					if (resolve) {
						resolveClass(clazz);
					}
					loadedClasses.put(name, clazz);
				}
			}
			return clazz;
		}

		@Override
		public InputStream getResourceAsStream(String name) {	
			return new ByteArrayInputStream(getResourceContent(name));
		}
		
		/**
		 * Reads the class bytes from different local and remote resources using
		 * ClasspathResources
		 * 
		 * @param name The <a href="#name">binary name</a> of the class.
		 * @return byte[]
		 */
		private byte[] loadClassBytes(String name) {	
			return getResourceContent(ClassNameUtil.classNameToPathName(name));
		}
	}
	
	private final class CurrentClassLoader implements ProxyClassLoader {

		@Override
		public URL getResource(String name) {
			return getClass().getClassLoader().getResource(name);
		}
		
		@Override
		public Class<?> loadClass(String name) {
			return loadClass(name, true);
		}

		@Override
		public Class<?> loadClass(String name, boolean resolve) {
			Class<?> clazz;
			try {
				clazz = getClass().getClassLoader().loadClass(name);
			} catch (ClassNotFoundException e) {
				return null;
			}
			return clazz;
		}

		@Override
		public InputStream getResourceAsStream(String name) {
			return getClass().getClassLoader().getResourceAsStream(name);
		}
	}
	
	private final class ParentClassLoader implements ProxyClassLoader {

		@Override
		public URL getResource(String name) {
			return getParent() != null ? getParent().getResource(name) : null;
		}
		
		@Override
		public Class<?> loadClass(String name) {
			return loadClass(name, true);
		}

		@Override
		public Class<?> loadClass(String name, boolean resolve) {
			Class<?> clazz;
			try {
				clazz = getParent() != null ? getParent().loadClass(name) : null;
			} catch (ClassNotFoundException | NullPointerException e) {
				return null;
			}
			return clazz;
		}

		@Override
		public InputStream getResourceAsStream(String name) {
			return getParent() != null ? getParent().getResourceAsStream(name): null;
		}
		
	}
	
	private final class ThreadContextClassLoader implements ProxyClassLoader {

		@Override
		public URL getResource(String name) {
			return Thread.currentThread().getContextClassLoader().getResource(name);
		}
		
		@Override
		public Class<?> loadClass(String name) {
			return loadClass(name, true);
		}

		@Override
		public Class<?> loadClass(String name, boolean resolve) {

			Class<?> clazz;
			try {
				clazz = Thread.currentThread().getContextClassLoader().loadClass(name);
			} catch (ClassNotFoundException e) {
				return null;
			}
			return clazz;
		}

		@Override
		public InputStream getResourceAsStream(String name) {
			return Thread.currentThread().getContextClassLoader().getResourceAsStream(name);
		}

	}
	
	private final class SystemClassLoader implements ProxyClassLoader {

		@Override
		public URL getResource(String name) {			
			return getSystemResource(name);
		}
		
		@Override
		public Class<?> loadClass(String name) {
			return loadClass(name, true);
		}

		@Override
		public Class<?> loadClass(String name, boolean resolve) {

			Class<?> result;
			try {
				result = findSystemClass(name);
			} catch (ClassNotFoundException e) {
				return null;
			}
			return result;
		}

		@Override
		public InputStream getResourceAsStream(String name) {		
			return getSystemResourceAsStream(name);
		}
	}
	
	private final ProxyClassLoader bootLoader;			
	private final ProxyClassLoader localLoader;
	private final ProxyClassLoader currentLoader;
	private final ProxyClassLoader parentLoader;	
	private final ProxyClassLoader theadLoader;	
	private final ProxyClassLoader systemLoader;

	private final Properties properties;
	
	private final Map<String, Class<?>> loadedClasses;
	
	private Boolean autoProxyEnabled;

	/**
	 * Create new default bundle class loader.
	 * 
	 * @param name - Name for the bundle loader
	 */
	public BundleClassLoaderImpl(String name) {		
		this(name, null, null, new String[0]);
	}
	
	/**
	 * Create new bundle class loader with specified boot packages.
	 * 
	 * @param name - Name for the bundle loader
	 * @param bootPackages - Boot loader packages
	 */
	public BundleClassLoaderImpl(String name, String... bootPackages) {		
		this(name, null, null, bootPackages);
	}
	
	/**
	 * Create new bundle class loader with specified parent class loader.
	 * 
	 * @param name - Name for the bundle loader
	 * @param parent - The optional parent class loader to use
	 */
	public BundleClassLoaderImpl(String name, ClassLoader parent) {
		this(name, parent, null, new String[0]);
	}
	
	/**
	 * Create new bundle class loader with parent class loader and specified boot packages.
	 * 
	 * @param name - Name for the bundle loader
	 * @param parent - The optional parent class loader to use
	 * @param bootPackages - Boot loader packages
	 */
	public BundleClassLoaderImpl(String name, ClassLoader parent, String... bootPackages) {
		this(name, parent, null, bootPackages);
	}
	
	/**
	 * Create new bundle class loader with specified configuration properties.
	 * 
	 * @param name - Name for the bundle loader
	 * @param properties - Configuration properties
	 */
	public BundleClassLoaderImpl(String name, Properties properties) {		
		this(name, null, properties);
	}
	
	/**
	 * Create new bundle class loader with specified configuration properties and boot packages.
	 * 
	 * @param name - Name for the bundle loader
	 * @param properties - Configuration properties
	 * @param bootPackages - Boot loader packages
	 */
	public BundleClassLoaderImpl(String name, Properties properties, String... bootPackages) {		
		this(name, null, properties, bootPackages);
	}
	
	/**
	 * Create new bundle class loader with specified configuration properties and parent class loader.
	 * 
	 * @param name - Name for the bundle loader
	 * @param parent - The optional parent class loader to use
	 * @param properties - Configuration properties
	 */
	public BundleClassLoaderImpl(String name, ClassLoader parent, Properties properties) {
		this(name, parent, properties, new String[0]);
	}
	
	/**
	 * Create new bundle class loader with specified configuration properties, parent class loader and boot loader packages.
	 * 
	 * @param name - Name for the bundle loader
	 * @param parent - The optional parent class loader to use
	 * @param properties - Configuration properties
	 * @param bootPackages - Boot loader packages
	 */
	public BundleClassLoaderImpl(String name, ClassLoader parent, Properties properties, String... bootPackages) {
		super(name, parent);
		autoProxyEnabled = null;
		this.properties = properties != null ? properties : PropertyUtil.loadProperties();
		this.loadedClasses = Collections.synchronizedMap(new HashMap<String, Class<?>>());		
		bootLoader = new BootClassLoader(bootPackages);
		localLoader = new LocalClassLoader();
		currentLoader = new CurrentClassLoader();
		parentLoader = new ParentClassLoader();
		theadLoader = new ThreadContextClassLoader();
		systemLoader = new SystemClassLoader();
		initializeProxyClassLoaders();
		logger.info("name={}, message=New bundle class loader initialized with name: {}.", getName(), getName());		
	}
	
	@Override
	public void addResource(byte[] content) throws IOException {
		loadResourceFromBytes(content);	
	}
	
	@Override
	public void addResource(File file) throws IOException {
		loadResourceFromFile(file);
	}
	
	@Override
	public void addResource(URL url) throws IOException {
		loadResourceFromUrl(url);	
	}
	
	@Override
	public void addResources(List<Object> sources) throws IOException {
		for(Object source : sources) {
			addResource(source);
		}
	}
	
	@Override
	public Object create(String name) {
		return create(name, (Object[]) null);
	}
	
	@Override
	public Object create(String name, Object... args) {
		if(args == null || args.length == 0) {
			try {
				Object obj = loadClass(name).newInstance();
				return isAutoProxyEnabled() ? createProxyObject(obj) : obj;
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				throw new BundleClassLoaderException(String.format("Failed to create object by invoking default constructor. Reason: %s", 
						e.getMessage()), e);
			}
		}
		Class<?>[] types = new Class[args.length];
		for (int i = 0; i < args.length; i++){
			types[i] = args[i].getClass();
		}
		return create(name, args, types);
	}

	@Override
	public Object create(String name, Object[] args, Class<?>[] types) {	
		Object obj = null;
		if (args == null || args.length == 0) {
			try {
				obj = loadClass(name).newInstance();
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
				throw new BundleClassLoaderException(String.format("Failed to create object by invoking default constructor. Reason: %s", 
						e.getMessage()), e);
			}
		} else {
			try {
				obj = loadClass(name).getConstructor(types).newInstance(args);
			} catch (Exception e) {
				throw new BundleClassLoaderException(String.format("Failed to create object by invoking constructor with parameters. "
						+ "Reason: %s", e.getMessage()), e);
			}
		}
		return isAutoProxyEnabled() ? createProxyObject(obj) : obj;
	}

	@Override
	public Object create(String name, String methodName, Object... args) {
		if (args == null || args.length == 0) {
			try {
				Object obj = loadClass(name).getMethod(methodName).invoke(null);
				return isAutoProxyEnabled() ? createProxyObject(obj) : obj;
			} catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InvocationTargetException 
					| NoSuchMethodException | SecurityException e) {
				throw new BundleClassLoaderException(String.format("Failed to create object by invoking default static factory method. "
						+ "Reason: %s", e.getMessage()), e);
			}
		}
		
		Class<?>[] types = new Class[args.length];
		for (int i = 0; i < args.length; i++){
			types[i] = args[i].getClass();
		}
		return create(name, methodName, args, types);
	}

	@Override
	public Object create(String name, String methodName, Object[] args, Class<?>[] types) {	
		Object obj = null;
		if (args == null || args.length == 0) {
			try {
				obj = loadClass(name).getMethod(methodName).invoke(null);
			} catch (ClassNotFoundException | IllegalAccessException | IllegalArgumentException | InvocationTargetException 
					| NoSuchMethodException | SecurityException e) {
				throw new BundleClassLoaderException(String.format("Failed to create object by invoking default static factory method. "
						+ "Reason: %s", e.getMessage()), e);
			}
		} else {
			try {
				obj = loadClass(name).getMethod(methodName, types).invoke(null, args);
			} catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException 
					| SecurityException | ClassNotFoundException e) {
				throw new BundleClassLoaderException(String.format("Failed to create object by invoking parametrized static factory method."
						+ "Reason: %s", e.getMessage()), e);
			}
		}
		return isAutoProxyEnabled() ? createProxyObject(obj) : obj;
	}
	
	@Override
	public List<String> getLoadedClassNames() {
		return new ArrayList<>(loadedClasses.keySet());
	}
	
	@Override
	public boolean isAutoProxyEnabled() {
		if(autoProxyEnabled == null) {
			autoProxyEnabled = Boolean.parseBoolean(properties.getProperty("relaysoft.bundle.loader.proxy.auto", "false"));
		}
		return autoProxyEnabled;
	}
	
	@Override
	public void setAutoProxyEnabled(boolean enable) {
		this.autoProxyEnabled = enable;
	}
	
	@Override
	public void unloadAllClasses(){
		logger.debug("name={}, message=Unloading all loaded classes from bundle class loader.", getName());
		List<String> names = new ArrayList<>(loadedClasses.keySet());
		names.stream().forEach(this::unloadClass);
	}
	
	@Override
	public void unloadClass(String className) {
		logger.trace("name={}, message=Unloading class {} from bundle class loader.", getName(), className);
		loadedClasses.remove(className);
	}
	
	private void addResource(Object source) throws IOException {
		if(source instanceof byte[]) {
			addResource((byte[]) source);
		} else if(source instanceof File) {
			addResource((File) source);
		} else if(source instanceof URL) {
			addResource((URL) source);
		} else {
			throw new IllegalArgumentException(String.format("Adding JAR from parameter [%s] is not supported!", 
					source.getClass().getName()));
		}
	}
	
	private void initializeProxyClassLoaders() {	
		addProxyClassLoader(bootLoader);
		addProxyClassLoader(localLoader);
		addProxyClassLoader(currentLoader);
		addProxyClassLoader(parentLoader);
		addProxyClassLoader(theadLoader);
		addProxyClassLoader(systemLoader);
	}

}
